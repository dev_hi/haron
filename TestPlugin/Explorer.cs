﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Plugin;
using Haron.Server.Http.MiddleWare;
using Haron.Server.Http.Packet;

namespace TestPlugin {
	[TypeExport()]
	public class Explorer : IMiddleWare {
		public string GetDescription() {
			return "TestPlugin";
		}

		public void OnCreate(string uri) {

		}

		public bool OnException(Exception e) {
			return false;
		}

		public bool OnResponse(HttpRequest req, HttpResponse res) {
			string[] test = req.InURI.Split(new char[] { '/' });
			string html = Properties.Resources.explorer;
			string path = "C:\\";
			
			for (int i = 0; i < test.Length; ++i) {
				path = Path.Combine(path, System.Web.HttpUtility.UrlDecode(test[i]));
			}
			if(File.Exists(path)) {
				res.SendFile(path);
			}
			else if(Directory.Exists(path)) {
				string[] d = Directory.GetDirectories(path);
				string[] f = Directory.GetFiles(path);
				string h = "";
				for(int i = 0; i < d.Length; ++i) {
					string a = d[i].Replace(path, "");
					h += "<p><a href=\"" + req.URI + (a[0] == '/' ? "" : "/") +a + "\">" + a + "</a></p>";
				}
				for(int i = 0; i < f.Length; ++i) {
					string a = f[i].Replace(path, "");
					h += "<p><a href=\"" + req.URI + (a[0] == '/' ? "" : "/") + a + "\">" + a + "</a></p>";
				}
				html = html.Replace("{PATH}", path);
				html = html.Replace("{LIST}", h);
				res.Send(Encoding.UTF8, html);
			}
			else {
				res.NotFound();
			}
			return false;
		}
	}
}
