﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace WQAPI.SystemQuery.WMI {
	public class Processor {
		public UInt16 AddressWidth;
		public UInt16 Architecture;
		public string AssetTag;
		public UInt16 Availability;
		public string Caption;
		public UInt32 Characteristics;
		public UInt16 CpuStatus;
		public string CreationClassName;
		public UInt32 CurrentClockSpeed;
		public UInt16 CurrentVoltage;
		public UInt16 DataWidth;
		public string Description;
		public UInt32 ExtClock;
		public UInt16 Family;
		public UInt32 L2CacheSize;
		public UInt32 L3CacheSize;
		public UInt32 L3CacheSpeed;
		public UInt16 Level;
		public UInt16 LoadPercentage;
		public string Manufacturer;
		public UInt32 MaxClockSpeed;
		public string Name;
		public UInt32 NumberOfCores;
		public UInt32 NumberOfEnabledCore;
		public UInt32 NumberOfLogicalProcessors;
		public string PartNumber;
		public string PNPDeviceID;
		public bool PowerManagementSupported;
		public string ProcessorId;
		public UInt16 ProcessorType;
		public UInt16 Revision;
		public string Role;
		public bool SecondLevelAddressTranslationExtensions;
		public string SerialNumber;
		public string SocketDesignation;
		public string Status;
		public UInt16 StatusInfo;
		public string SystemCreationClassName;
		public string SystemName;
		public UInt32 ThreadCount;
		public UInt16 UpgradeMethod;
		public string Version;
		public bool VirtualizationFirmwareEnabled;
		public bool VMMonitorModeExtensions;

		public Processor(ManagementObject query) {
			/*this.AddressWidth =				(UInt16)query["AddressWidth"];
			this.Architecture =					(UInt16)query["Architecture"];
			this.AssetTag =						(string)query["AssetTag"];
			this.Availability =					(UInt16)query["Availability"];
			this.Caption =						(string)query["Caption"];
			this.Characteristics =				(UInt32)query["Characteristics"];
			this.CpuStatus =					(UInt16)query["CpuStatus"];
			this.CreationClassName =			(string)query["CreationClassName"];
			this.CurrentClockSpeed =			(UInt32)query["CurrentClockSpeed"];
			this.CurrentVoltage =				(UInt16)query["CurrentVoltage"];
			this.DataWidth =					(UInt16)query["DataWidth"];
			this.Description =					(string)query["Description"];
			this.ExtClock =						(UInt32)query["ExtClock"];
			this.Family =						(UInt16)query["Family"];
			this.L2CacheSize =					(UInt32)query["L2CacheSize"];
			this.L3CacheSize =					(UInt32)query["L3CacheSize"];
			this.L3CacheSpeed =					(UInt32)query["L3CacheSpeed"];
			this.Level =						(UInt16)query["Level"];
			this.LoadPercentage =				(UInt16)query["LoadPercentage"];
			this.Manufacturer =					(string)query["Manufacturer"];
			this.MaxClockSpeed =				(UInt32)query["MaxClockSpeed"];
			this.Name =							(string)query["Name"];
			this.NumberOfCores =				(UInt32)query["NumberOfCores"];
			this.NumberOfEnabledCore =			(UInt32)query["NumberOfEnabledCore"];
			this.NumberOfLogicalProcessors =	(UInt32)query["NumberOfLogicalProcessors"];
			this.PartNumber =					(string)query["PartNumber"];
			this.PNPDeviceID =					(string)query["PNPDeviceID"];*/
		}
	}
}