﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace WQAPI.SystemQuery.WMI {
	public class Memory {

		private static ulong total_memory = 0;
		public static ulong TotalPhysicalMemory {
			get {
				if (total_memory == 0) GetInstalledMemory();
				return total_memory;
			}
		}
		private static Memory[] InstalledMemory = null;

		public static Memory[] FromQuery(ManagementObject[] query) {
			Memory[] memories = new Memory[query.Length];
			for (int i = 0; i < query.Length; ++i) {
				memories[i] = new Memory(query[i]);
				memories[i].Banks = memories;
				total_memory += memories[i].Capacity;
			}
			return memories;
		}

		private static Memory[] GetInstalledMemory() {
			if (InstalledMemory == null) InstalledMemory = FromQuery(WmiUtils.GetInstances("root\\CIMV2", "Win32_PhysicalMemory"));
			return InstalledMemory;
		}


		public Memory[] Banks;
		public UInt32 Attributes;
		public string BankLabel;
		public UInt64 Capacity;
		public string Caption;
		public UInt32 ConfiguredClockSpeed;
		public UInt32 ConfiguredVoltage;
		public string CreationClassName;
		public UInt16 DataWidth;
		public string Description;
		public string DeviceLocator;
		public UInt16 FormFactor;
		public bool	HotSwappable;
		public DateTime InstallDate;
		public UInt16 InterleaveDataDepth;
		public UInt32 InterleavePosition;
		public string Manufacturer;
		public UInt32 MaxVoltage;
		public UInt16 MemoryType;
		public UInt32 MinVoltage;
		public string Model;
		public string Name;
		public string OtherIdentifyingInfo;
		public string PartNumber;
		public UInt32 PositionInRow;
		public bool PoweredOn;
		public bool Removable;
		public bool Replaceable;
		public string SerialNumber;
		public string SKU;
		public UInt32 SMBIOSMemoryType;
		public UInt32 Speed;
		public string Status;
		public string Tag;
		public UInt16 TotalWidth;
		public UInt16 TypeDetail;
		public string Version;

		public Memory(ManagementObject query) {
			this.Attributes =			(UInt32)query["Attributes"];
			this.BankLabel =			(string)query["BankLabel"];
			this.Capacity =				(UInt64)query["Capacity"];
			this.Caption =				(string)query["Caption"];
			this.ConfiguredClockSpeed =	(UInt32)query["ConfiguredClockSpeed"];
			this.ConfiguredVoltage =	(UInt32)query["ConfiguredVoltage"];
			this.CreationClassName =	(string)query["CreationClassName"];
			this.DataWidth =			(UInt16)query["DataWidth"];
			this.Description =			(string)query["Description"];
			this.DeviceLocator =		(string)query["DeviceLocator"];
			this.FormFactor =			(UInt16)query["FormFactor"];
			this.InterleaveDataDepth =	(UInt16)query["InterleaveDataDepth"];
			this.InterleavePosition =	(UInt32)query["InterleavePosition"];
			this.Manufacturer =			(string)query["Manufacturer"];
			this.MaxVoltage =			(UInt32)query["MaxVoltage"];
			this.MemoryType =			(UInt16)query["MemoryType"];
			this.MinVoltage =			(UInt32)query["MinVoltage"];
			this.Name =					(string)query["Name"];
			this.PartNumber =			(string)query["PartNumber"];
			this.SerialNumber =			(string)query["SerialNumber"];
			this.SMBIOSMemoryType =		(UInt32)query["SMBIOSMemoryType"];
			this.Speed =				(UInt32)query["Speed"];
			this.Tag =					(string)query["Tag"];
			this.TotalWidth =			(UInt16)query["TotalWidth"];
			this.TypeDetail =			(UInt16)query["TypeDetail"];
		}
	}
}
