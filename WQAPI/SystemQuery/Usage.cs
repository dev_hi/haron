﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Management;
using WQAPI.SystemQuery.WMI;

namespace WQAPI.SystemQuery {
	public class Usage {
		private static PerformanceCounter ProcessorCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
		private static PerformanceCounter MemoryCounter = new PerformanceCounter("Memory", "Available MBytes");
		
		public static float MemoryUsage {
			get {
				return ((float)UsingMemory / (float)TotalMemoryCapacity);
			}
		}
		public static ulong TotalMemoryCapacity {
			get {
				return Memory.TotalPhysicalMemory;
			}
		}

		public static ulong AvailableMemory {
			get {
				return (ulong)(MemoryCounter.NextValue() * 1024 * 1024);
			}
		}

		public static ulong UsingMemory {
			get {
				return TotalMemoryCapacity - AvailableMemory;
			}
		}


		public static float ProcessorUsagePercent() {
			return ProcessorCounter.NextValue();
		}
		public static float MemoryUsagePercent() {
			return MemoryUsage * 100;
		}
	}
}
