﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WQAPI.SystemQuery {
	public class Processes {
		public static Process[] GetProcesses() {
			return Process.GetProcesses();
		}
		public static void KillProcess(int pid) {
			GetProcess(pid).Kill();
		}
		public static Process[] SearchProcesses(string name) {
			return Process.GetProcessesByName(name);
		}
		public static Process GetProcess(int pid) {
			return Process.GetProcessById(pid);
		}
		public static float GetProcessCpuPerformace(Process p) {
			return (new PerformanceCounter("Process", "% Processor Time", p.ProcessName, true)).NextValue();
		}
	}
}
