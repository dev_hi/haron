﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Threading.Tasks;
using Haron.Parser;

namespace WQAPI.SystemQuery {
	public static class WmiUtils {
		public static ManagementObject[] GetInstances(string Namespace, string Class) {
			ManagementObjectCollection moc = new ManagementObjectSearcher(Namespace, "SELECT * FROM " + Class).Get();
			ManagementObject[] wmi_result = new ManagementObject[moc.Count];
			moc.CopyTo(wmi_result, 0);
			return wmi_result;
		}
		public static JSON GetInstancesToJSON(string Namespace, string Class) {
			ManagementObject[] wmi_result = GetInstances(Namespace, Class);
			JSON s = new JSON(JsonObjectType.array);
			for(int i = 0; i < wmi_result.Length; ++i) {
				s[i] = new JSON();
				foreach(PropertyData data in wmi_result[i].Properties) {
					if(data.Value != null) {
						s[i][data.Name] = new JSON(data.Value);
					}
				}
			}
			return s;
		}
	}
}
