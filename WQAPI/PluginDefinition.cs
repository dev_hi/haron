﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Haron.Parser;
using Haron.Plugin;
using Haron.Server.Http.MiddleWare;

namespace WQAPI {
    [HaronPlugin("55E96521-7915-4F2D-953E-1AED71B6DC48", "Windows Query API over HTTP", "0.1.0.0", Author = "Haron", Description = "", LoadOnApplicationOnly = false)]
    public static class PluginDefinition {
		public static DateTime POSIX = new DateTime(1970, 1, 1);
		public static RestAPI RestAPIEngine = new RestAPI();

		[PluginCallback(PluginCallbackType.OnPluginLoad)]
        public static void startup(string[] args) {
			/*HttpServer Server = (HttpServer)Initializer.Servers[0];
			HttpServer Server = new HttpServer(new HttpSetupInformation());
			RestAPIEngine.SetEndpoint(HttpMethod.GET, "/usages", ((req, res, args) => {
				JSON s = new JSON();
				s["cpu"] = new JSON(Usage.ProcessorUsagePercent());
				s["ram"] = new JSON(Usage.MemoryUsagePercent());
				return s;
			}));
			RestAPIEngine.SetEndpoint(HttpMethod.GET, "/processes", ((req, res, args) => {
				JSON s = new JSON();
				s["processes"] = new JSON(JsonObjectType.array);
				Process[] processes = Processes.GetProcesses();
				for(int i = 0; i < processes.Length; ++i) {
					try {
						JSON temp = new JSON(JsonObjectType.@object);
						temp["PID"] = new JSON(processes[i].Id);
						temp["MachineName"] = new JSON(processes[i].MachineName);
						temp["ProcessName"] = new JSON(processes[i].ProcessName);
						temp["MainWindowTitle"] = new JSON(processes[i].MainWindowTitle);
						temp["UserName"] = new JSON(processes[i].StartInfo.UserName);
						temp["FileName"] = new JSON(processes[i].StartInfo.FileName);
						temp["Arguments"] = new JSON(processes[i].StartInfo.Arguments);
						temp["Threads"] = new JSON(processes[i].Threads.Count);
						temp["WorkingSet64"] = new JSON(processes[i].WorkingSet64);
						temp["VirtualMemorySize64"] = new JSON(processes[i].VirtualMemorySize64);
						temp["PagedMemorySize64"] = new JSON(processes[i].PagedMemorySize64);
						temp["NonpagedSystemMemorySize64"] = new JSON(processes[i].NonpagedSystemMemorySize64);
						temp["PrivateMemorySize64"] = new JSON(processes[i].PrivateMemorySize64);
						//temp["CpuUsage"] = new JSON(Processes.GetProcessCpuPerformace(processes[i]));
						s["processes"][i] = temp;
					}
					catch(Exception) { }
				}
				return s;
			}));

			RestAPIEngine.SetEndpoint(HttpMethod.GET, "/explorer", ((req, res, args) => {
				JSON s = new JSON();
				string[] alliases = new string[Properties.Settings.Default.Alliases.Count];
				Properties.Settings.Default.Alliases.CopyTo(alliases, 0);
				string path = "";
				bool found = false;
				if(args.Length != 0) {
					for(int i = 0; i < alliases.Length; ++i) {
						string[] tmp = alliases[i].Split(new char[] { '=' }, StringSplitOptions.None);
						if(tmp[0] == args[0]) {
							found = true;
							path = tmp[1];
						}
					}
				}
				if(found) {
					for(int i = 1; i < args.Length; ++i) {
						path = Path.Combine(path, args[i]).TrimEnd('\\');
					}
					try {
						if((File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory) {
							DirectoryInfo current = new DirectoryInfo(path);
							s["currunt_directory"] = new JSON(current.FullName);
							s["currunt_name"] = new JSON(current.Name);
							s["contents"] = new JSON(JsonObjectType.@object);
							s["contents"]["directories"] = new JSON(JsonObjectType.array);
							s["contents"]["files"] = new JSON(JsonObjectType.array);

							DirectoryInfo[] directories = current.GetDirectories();
							FileInfo[] files = current.GetFiles();
							for(int i = 0; i < directories.Length; ++i) {
								try {
									JSON temp = new JSON(JsonObjectType.@object);
									temp["name"] = new JSON(directories[i].Name);
									temp["lastupdate"] = new JSON(directories[i].LastWriteTime.Subtract(POSIX).TotalMilliseconds);
									temp["lastaccess"] = new JSON(directories[i].LastAccessTime.Subtract(POSIX).TotalMilliseconds);
									temp["created"] = new JSON(directories[i].CreationTime.Subtract(POSIX).TotalMilliseconds);
									temp["directories"] = new JSON(directories[i].GetDirectories().Length);
									temp["files"] = new JSON(directories[i].GetFiles().Length);
									s["contents"]["directories"][i] = temp;
								}
								catch(UnauthorizedAccessException) { }
							}
							for(int i = 0; i < files.Length; ++i) {
								try {
									JSON temp = new JSON(JsonObjectType.@object);
									temp["name"] = new JSON(files[i].Name);
									temp["lastupdate"] = new JSON(files[i].LastWriteTime.Subtract(POSIX).TotalMilliseconds);
									temp["lastaccess"] = new JSON(files[i].LastAccessTime.Subtract(POSIX).TotalMilliseconds);
									temp["created"] = new JSON(files[i].CreationTime.Subtract(POSIX).TotalMilliseconds);
									temp["size"] = new JSON(files[i].Length);
									s["contents"]["files"][i] = temp;
								}
								catch(UnauthorizedAccessException) { }
							}
						}
						else {
							res.SendFile(path);
							s = null;
						}
					}
					catch(DirectoryNotFoundException) {
						res.StatusCode = HttpResponseCode.NotFound;
						s["message"] = new JSON("not found");
					}
					catch(FileNotFoundException) {
						res.StatusCode = HttpResponseCode.NotFound;
						s["message"] = new JSON("not found");
					}
					catch(UnauthorizedAccessException) {
						res.StatusCode = HttpResponseCode.Unauthorized;
						s["message"] = new JSON("Unauthorized");
					}
				}
				else {
					res.StatusCode = HttpResponseCode.NotFound;
					s["message"] = new JSON("the file or directories not found");
				}
				return s;
			}));
			RestAPIEngine.SetEndpoint(HttpMethod.POST, "/killprocess", (req, res, args) => {
				JSON s = new JSON();
				if(args.Length > 0) {
					int pid;
					if(int.TryParse(args[0], out pid)) {
						try {
							Processes.KillProcess(pid);
							s["success"] = new JSON(true);
						}
						catch(Win32Exception e) {
							res.StatusCode = HttpResponseCode.Forbidden;
							s["errorcode"] = new JSON(e.ErrorCode);
							s["native_errorcode"] = new JSON(e.NativeErrorCode);
							s["message"] = new JSON(e.Message);
						}
					}
					else {
						res.StatusCode = HttpResponseCode.BadRequest;
						s["message"] = new JSON("wrong request");
					}
				}
				else {
					res.StatusCode = HttpResponseCode.BadRequest;
					s["message"] = new JSON("wrong request");
				}
				return s;
			});
			RestAPIEngine.SetEndpoint(HttpMethod.GET, "/syssum", (req, res, args) => {
				JSON s = new JSON();
				s["Win32_Processor"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_Processor");
				s["Win32_ComputerSystem"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_ComputerSystem");
				s["Win32_PhysicalMemory"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_PhysicalMemory");
				s["Win32_SystemEnclosure"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_SystemEnclosure");
				s["Win32_DiskDrive"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_DiskDrive");
				s["Win32_DiskPartition"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_DiskPartition");
				s["Win32_DiskDriveToDiskPartition"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_DiskDriveToDiskPartition");
				s["Win32_LogicalDisk"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_LogicalDisk");
				s["Win32_LogicalDiskToPartition"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_LogicalDiskToPartition");
				s["Win32_NetworkAdapter"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_NetworkAdapter");
				s["Win32_NetworkAdapterConfiguration"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_NetworkAdapterConfiguration");
				s["Win32_Battery"] = WmiUtils.GetInstancesToJSON("root\\CIMV2", "Win32_Battery");
				return s;
			});
			RestAPIEngine.SetEndpoint(HttpMethod.GET, "/wmi_query", (req, res, args) => {
				JSON s = new JSON();
				try {
					if(args.Length < 2) {
						res.StatusCode = HttpResponseCode.BadRequest;
						s["message"] = new JSON("wrong request");
					}
					else {
						s[args[1]] = WmiUtils.GetInstancesToJSON("root\\" + args[0], args[1]);
					}
				}
				catch(ManagementException) {
					res.StatusCode = HttpResponseCode.NotFound;
					s["message"] = new JSON("not found or wrong classname");
				}
				return s;
			});
			Server.Router.RegisterMiddleWare("/", new HttpAuth(new BasicAuthorization("admin", "1q2w3e4r")));
			Server.Router.RegisterMiddleWare("/", new Default());
			Server.Router.RegisterMiddleWare("/test", new Default(".\\www\\template"));
			Server.Router.RegisterMiddleWare("/javascripts", new Default());
			Server.Router.RegisterMiddleWare("/images", new Default());
			Server.Router.RegisterMiddleWare("/fonts", new Default());
			Server.Router.RegisterMiddleWare("/stylesheets", new Default());
			Server.Router.RegisterMiddleWare("/api", new HttpAuth(new BasicAuthorization("api", "1")));
			Server.Router.RegisterMiddleWare("/api", RestAPIEngine);*/
		}
    }
}
