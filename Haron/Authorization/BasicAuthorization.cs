﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Authorization {
	public class BasicAuthorization : IAuthentication {
		private string username = "";
		private string password = "";

		public BasicAuthorization(string username, string password) {
			this.username = username;
			this.password = password;
		}

		public bool Authenticate(string username, string password) {
			return (username == this.username && this.password == password);
		}
	}
}
