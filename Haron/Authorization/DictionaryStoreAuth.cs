﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Authorization {
	public class DictionaryStoreAuth : IAuthentication {
		private Dictionary<string, string> Users = new Dictionary<string, string>();
		public DictionaryStoreAuth() { }
		public DictionaryStoreAuth(Dictionary<string, string> storage) {
			this.Users = storage;
		}

		public void AddUser(string username, string password) {
			if (this.Users.ContainsKey(username)) this.Users[username] = password;
			else this.Users.Add(username, password);
		}

		public bool Authenticate(string username, string password) {
			if(this.Users.ContainsKey(username)) return (this.Users[username] == password);
			return false;
		}

		public bool UserExists(string username) {
			return this.Users.ContainsKey(username);
		}
	}
}
