﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Haron.Plugin.Internal;
using System.Windows.Automation.Peers;
using System.CodeDom;

namespace Haron.Plugin {
	public delegate void PluginLoadCallback(PluginLoader pl);
	public class PluginLoader {
		public Assembly LoadedAssembly { get; private set; }
		public AssemblyName LoadedAssemblyName { get; private set; }
		public HaronPluginAttribute PluginAttribute { get; private set; }
		public bool LoadSuccess { get; private set; }
		public Exception LoadFailedException { get; private set; }
		public PluginDependancyInfo[] DependancyInfo => this.PluginAttribute.DependancyInfo;
		public string Name => this.PluginAttribute.Name;
		public string Author => this.PluginAttribute.Author;
		public string Description => this.PluginAttribute.Description;
		public Guid GUID => this.PluginAttribute.GUID;
		public Version Version => this.PluginAttribute.Version;
		public bool LoadOnApplicationOnly => this.PluginAttribute.LoadOnApplicationOnly;

		public string DLL { get; private set; }

		internal PluginLoader(string dllpath, HaronPluginAttribute plugin_attr, Assembly loaded, string[] args, bool isApp, PluginLoadCallback cb = null, MethodInfo load_callback = null) {
			try {
				this.DLL = dllpath;
				this.LoadSuccess = false;
				this.LoadedAssembly = loaded;
				this.LoadedAssemblyName = this.LoadedAssembly.GetName();
				this.PluginAttribute = plugin_attr;
				if(!(this.LoadOnApplicationOnly && !isApp)) {
					if(PluginManager.CheckDependancy(DependancyInfo)) {
						load_callback?.Invoke(null, new object[] { args });
						this.LoadSuccess = true;
					}
					else {
						this.LoadFailedException = new DependancyNotLoadedException();
					}
				}
			}
			catch(Exception e) {
				this.LoadFailedException = e;
			}
			cb?.Invoke(this);
		}
	}
}
