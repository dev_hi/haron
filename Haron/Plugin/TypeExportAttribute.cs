﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public class TypeExportAttribute : Attribute {
		
	}
}
