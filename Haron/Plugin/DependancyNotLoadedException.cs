﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
	public class DependancyNotLoadedException : Exception {
		public string Message { get; private set; }
		public PluginDependancyInfo DependancyInfo;

		public DependancyNotLoadedException() {
			this.Message = "종속성 플러그인중 하나가 로드되지 않았습니다.";
		}
	}
}
