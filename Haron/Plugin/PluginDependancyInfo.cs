﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
	public class PluginDependancyInfo {
		public Guid GUID { get; private set; }
		public string Name { get; private set; }
		public Version Version { get; private set; }
		public bool Loaded { get; internal set; }

		public PluginDependancyInfo(string name, Version version) {
			this.Name = name;
			this.Version = version;
		}
		public PluginDependancyInfo(Guid GUID, Version version) {
			this.GUID = GUID;
			this.Version = version;
		}
	}
}
