﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using Haron.Authorization;
using Haron.Parser;
using Haron.Plugin;
using Haron.Server.Http;
using Haron.Server.Http.MiddleWare;
using Haron.Server.Http.Packet;

namespace Haron.Plugin.Internal {
	[HaronPlugin("PluginCallbackMethod", "Basic Plugin", "0.1.1.1", Author = "Haron", Description = "this is basic plugin.", LoadOnApplicationOnly = false)]
	public class BasicPlugin {

		public void OnPluginLoad(string[] argv) {
			/**/
		}
	}
}
