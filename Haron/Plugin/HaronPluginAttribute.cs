﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class HaronPluginAttribute : Attribute {
        public PluginDependancyInfo[] DependancyInfo = new PluginDependancyInfo[0];
        public Guid GUID { get; private set; }
        public string Name { get; private set; }
        public string Description = "";
        public Version Version { get; private set; }
        public string Author = "";
        public bool LoadOnApplicationOnly = false;

        public HaronPluginAttribute(Guid guid, string name, Version version) {
            this.GUID = guid;
            this.Name = name;
            this.Version = version;
        }
        public HaronPluginAttribute(string guid, string name, string version) : this(new Guid(guid), name, new Version(version)) { }
    }
}
