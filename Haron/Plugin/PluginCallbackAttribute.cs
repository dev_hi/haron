﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
	public enum PluginCallbackType {
		OnPluginLoad
	}
	[AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
	public class PluginCallbackAttribute : Attribute {

		public PluginCallbackType CallbackType { get; private set; }

		public PluginCallbackAttribute(PluginCallbackType method) {
			this.CallbackType = method;
			
		}
	}
}
