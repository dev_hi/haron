﻿using Haron.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Plugin {
	public static class PluginManager {
		/*public static Type[] InternalPluginList = new Type[] {
			typeof(BasicPlugin)
		};*/

		public static List<PluginLoader> LoadedPlugin = new List<PluginLoader>();
		private static List<Type> _importedType = new List<Type>();

		public static void LoadPlugins(string[] files, string[] args, bool isApp, PluginLoadCallback cb = null) {
			for(int i = 0; i < files.Length; ++i) {
				if(files[i].Substring(files[i].LastIndexOf('.') + 1, 3).ToLower() == "dll")
					LoadedPlugin.AddRange(LoadModule(files[i], args, isApp, cb));
			}
		}

		/*public static void LoadInternalPlugins(bool isApplication, string[] args) {
			foreach(Type type in InternalPluginList) {
				IPluginInterface plugin = ((IPluginInterface)Activator.CreateInstance(type));
				if (!(plugin.LoadOnApplicationOnly && !isApplication)) {
					plugin.OnPluginLoad(args);
				}
			}
		}*/

		public static PluginLoader[] LoadModule(string dllpath, string[] args, bool isApp, PluginLoadCallback cb = null) {
			Type tpatt = typeof(HaronPluginAttribute);
			Type tcatt = typeof(PluginCallbackAttribute);
			Type ttatt = typeof(TypeExportAttribute);
			List<PluginLoader> loader = new List<PluginLoader>();
			AssemblyName LoadedAssemblyName = AssemblyName.GetAssemblyName(dllpath);
			Assembly LoadedAssembly = Assembly.Load(LoadedAssemblyName);
			Type[] tp = LoadedAssembly.GetTypes();
			bool attr_found = false;
			bool plugindesc_found = false;
			for(int i = 0; i < tp.Length; ++i) {
				if(plugindesc_found && Attribute.IsDefined(tp[i], ttatt)) {
					_importedType.Add(tp[i]);
				}
				if(Attribute.IsDefined(tp[i], tpatt)) {
					plugindesc_found = true;
					IEnumerator<HaronPluginAttribute> plugininfo = tp[i].GetCustomAttributes<HaronPluginAttribute>().GetEnumerator();
					while(plugininfo.MoveNext()) {
						MethodInfo[] staticmethods = tp[i].GetMethods(BindingFlags.Public | BindingFlags.Static);
						int j = 0;
						bool found = false;
						for(; j < staticmethods.Length; ++j) {
							if(Attribute.IsDefined(staticmethods[j], tcatt)) {
								PluginCallbackAttribute cbattr = staticmethods[j].GetCustomAttributes<PluginCallbackAttribute>().First();
								if(cbattr.CallbackType == PluginCallbackType.OnPluginLoad) {
									found = true;
									break;
								}
							}
						}
						bool overlapped = false;
						for(int k = 0; k < LoadedPlugin.Count; ++k) {
							if(LoadedPlugin[k].GUID.Equals(plugininfo.Current.GUID)) {
								overlapped = true;
								break;
							}
						}
						if(!overlapped) {
							if(found) loader.Add(new PluginLoader(dllpath, plugininfo.Current, LoadedAssembly, args, isApp, cb, staticmethods[j]));
							else loader.Add(new PluginLoader(dllpath, plugininfo.Current, LoadedAssembly, args, isApp, cb));
						}
					}
				}
			}
			if(attr_found) {
				ServerManager.AddCreatableServer(_importedType.ToArray());
			}
			return loader.ToArray();
		}

		public static bool CheckDependancy(PluginDependancyInfo[] DependancyInfo) {
			bool ret = true;
			foreach(PluginDependancyInfo pdi in DependancyInfo) {
				bool found = false;
				if(pdi.GUID != null) {
					foreach(PluginLoader p in LoadedPlugin) if(p.GUID == pdi.GUID) {
						pdi.Loaded = found = true;
						break;
					}
				}
				else if(pdi.Name != null) {
					foreach(PluginLoader p in LoadedPlugin) if(p.Name == pdi.Name) {
						pdi.Loaded = found = true;
						break;
					}
				}
				if(!found) ret = false;
			}
			return ret;
		}

		public static Type[] GetImportedClasses(Type search = null) {
			return GetImportedClasses(search.FullName);
		}

		public static Type[] GetImportedClasses(string interface_name = null) {
			List<Type> ret = new List<Type>();
			for(int i = 0; i < _importedType.Count; ++i) {
				if(_importedType[i].GetInterface(interface_name) != null) ret.Add(_importedType[i]);
			}
			return ret.ToArray();
		}
	}
}
