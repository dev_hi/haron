﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Haron.Parser {
	public enum TagType {
		SingleTag,
		BlockTag,
		Text
	}

	public class HtmlDom {
		public TagType Type = TagType.Text;
		public string TagName;
		public HtmlDom Parent = null;
		public List<HtmlDom> children = new List<HtmlDom>();
		public Dictionary<string, string> Attributes = new Dictionary<string, string>();
		public int CurrentDepth;
		public int Line;
		public int Column;
		public string Text = "";
		
		public static List<HtmlDom> Parse(string html) {
			List<HtmlDom> ret = new List<HtmlDom>();

			//var

			HtmlDom p = null;
			HtmlDom c = null;
			int depth = 0;
			string text = "";
			string temptext = "";
			string name = "";
			TagType type = TagType.Text;

			//status
			bool n = false;
			bool t = false;
			bool e = false;

			for(int i = 0; i < html.Length; ++i) {
				char tk = html[i];
				temptext += tk;
				switch (tk) {
					case '<':
						if(t) {
							
						}
						else {
							t = true;
						}
						
						break;
					case '>':
						if(t) {
							
						}
						else {
							text += tk;
						}
						break;
					case '/':
						break;
					case '=':
						break;
					case '\"':
						break;
					case '\'':
						break;
					case ' ':
						if(t) {

						}
						else
							text += tk;
						break;
					default:
						text += tk;
						break;
				}
			}

			return ret;
		}

		public HtmlDom(TagType Type, string TagName) {

		}

		public HtmlDom(HtmlDom Parent, TagType Type, string TagName) {

		}

		public bool HasParent() {
			return (this.Parent != null);
		}

		public bool HasChild() {
			return (this.children.Count > 0);
		}

		public HtmlDom FirstChild() {
			if (!this.HasChild())
				return null;
			return this.children[0];
		}

		public HtmlDom LastChild() {
			if (!this.HasChild())
				return null;
			return this.children[this.children.Count - 1];
		}

		public HtmlDom PrevSibling() {
			return (this.Parent.IndexOf(this) == 0) ? this : this.Parent.children[this.Parent.IndexOf(this) - 1];
		}

		public HtmlDom NextSibling() {
			return (this.Parent.IndexOf(this) == this.Parent.children.Count - 1) ? this : this.Parent.children[this.Parent.IndexOf(this) + 1];
		}

		public HtmlDom FirstSibling() {
			return this.Parent.children[0];
		}

		public HtmlDom LastSibling() {
			return this.Parent.children[this.Parent.children.Count - 1];
		}

		public int IndexOf(HtmlDom findingNode) {
			if (!this.HasChild())
				return -1;
			else {
				int returnIndex = 0;
				bool still_finding = true;
				do {
					if (returnIndex == this.children.Count) {
						still_finding = false;
						returnIndex = -1;
					} else if (Object.ReferenceEquals(this.children[returnIndex], findingNode)) {
						still_finding = false;
					} else {
						returnIndex++;
					}
				} while (still_finding);

				return returnIndex;
			}
		}

		public HtmlDom Prepend(HtmlDom node) {
			if (node.HasParent())
				node.Parent.Remove(node);
			node.Parent = this;
			this.children.Insert(0, node);
			return node;
		}

		public HtmlDom PrependTo(HtmlDom node) {
			if (this.HasParent())
				this.Parent.Remove(this);
			this.Parent = node;
			node.children.Insert(0, this);
			return this;
		}

		public HtmlDom Append(HtmlDom node) {
			if (node.HasParent())
				node.Parent.Remove(node);
			node.Parent = this;
			this.children.Add(node);
			return node;
		}

		public HtmlDom AppendTo(HtmlDom node) {
			if (this.HasParent())
				this.Parent.Remove(this);
			this.Parent = node;
			node.children.Add(this);
			return this;
		}

		public HtmlDom Remove(HtmlDom node) {
			int i = this.IndexOf(node);
			if (i > -1) {
				this.children[i].Parent = null;
				this.children.RemoveAt(i);
			}
			return this;
		}

		public HtmlDom GetElementById(string id) {
			return this.Find("#" + id)[0];
		}

		public List<HtmlDom> GetElementsById(string id) {
			return this.Find("#" + id);
		}

		public List<HtmlDom> GetElementsByClassName(string classname) {
			return this.Find("." + classname);
		}

		public List<HtmlDom> GetElementsByTagName(string nodeName) {
			return this.Find(nodeName);
		}

		public string GetAttribute(string name) {
			return this.Attributes[name];
		}

		public void SetAttribute(string name, string value) {
			if (this.Attributes.ContainsKey(name)) {
				this.Attributes[name] = value;
			} else {
				this.Attributes.Add(name, value);
			}
		}
		
		/**
		
		*/

		public List<HtmlDom> Find(string selector) {
			List<HtmlDom> list = new List<HtmlDom>();

			if (this.HasChild()) {
				Regex RegularExp = new Regex(@"\[[^\]]*\]|\*|[#.\w]?[\w]*\w|[,>+~\s]*[,>+~\s]");
				MatchCollection matches = RegularExp.Matches(selector);
				List<HtmlDom> ChList = this.getNodeList();
				bool na = false;

				for (int i = 0; i <= matches.Count - 1; i++) {
					Match match = matches[i];
					string val = match.Value;

					if (val.Contains(".")) {
						List<HtmlDom> found = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							if (node.Attributes.ContainsKey("class")) {
								string[] splited = node.Attributes["class"].Split(' ');
								if (splited.ToList<string>().Contains(val.Substring(1))) {
									found.Add(node);
								}
							}
						}
						ChList = found;
						na = true;
					} else if (val.Contains("#")) {
						List<HtmlDom> found = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							if (node.Attributes.ContainsKey("id")) {
								if (node.Attributes["id"].Equals(val.Substring(1))) {
									found.Add(node);
								}
							}
						}
						ChList = found;
						na = true;
					} else if (val.Equals('*')) {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							foreach (HtmlDom node2 in node.getNodeList()) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = true;
					} else if (val.Contains(",") && na) {
						//쉼표 구분한 순서대로 리스트에 추가하는 버그
						this.combineNodeList(ChList, list);
						ChList = this.getNodeList();
						na = false;
					} else if (val.Equals(" ") && na) {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							foreach (HtmlDom node2 in node.getNodeList()) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = false;
					} else if (val.Contains(">") && na) {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							foreach (HtmlDom node2 in node.children) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = false;
					} else if (val.Contains("+") && na) {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							tmp.Add(node.NextSibling());
						}
						ChList = tmp;
						na = false;
					} else if (val.Contains("~") && na) {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							HtmlDom node2 = node.NextSibling();
							while (!Object.ReferenceEquals(node2, node2.LastSibling())) {
								tmp.Add(node2);
								node2 = node2.NextSibling();
							}
							tmp.Add(node2.NextSibling());
						}
						ChList = tmp;
						na = false;
					} else if (val[0] == '[' && val[val.Length - 1] == ']') {
						List<HtmlDom> tmp = new List<HtmlDom>();
						if (val.IndexOf("~=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "~=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key)) {
									string[] strarr = node.Attributes[key].Split(new char[] { ' ' });
									foreach (string str in strarr)
										if (str.Equals(value))
											tmp.Add(node);
								}
							}
						} else if (val.IndexOf("|=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "|=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key)) {
									string attrval = node.Attributes[key];
									if ((attrval.IndexOf("-" + value) > -1) || (attrval.IndexOf(value + "-") > -1) || (attrval.IndexOf("-" + value + "-") > -1))
										tmp.Add(node);
								}
							}
						} else if (val.IndexOf("^=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "^=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key)) {
									string attrval = node.Attributes[key];
									string start = attrval.Substring(0, value.Length);
									if (start.Equals(value))
										tmp.Add(node);
								}
							}
						} else if (val.IndexOf("$=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "$=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key)) {
									string attrval = node.Attributes[key];
									string end = attrval.Substring(attrval.Length - value.Length);
									if (end.Equals(value))
										tmp.Add(node);
								}
							}
						} else if (val.IndexOf("*=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "*=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key))
									if (node.Attributes[key].IndexOf(value) > -1)
										tmp.Add(node);
							}
						} else if (val.IndexOf("=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(key))
									if (node.Attributes[key] == value)
										tmp.Add(node);
							}
						} else {
							string splited = val.Substring(1, val.Length - 2);
							foreach (HtmlDom node in ChList) {
								if (node.Attributes.ContainsKey(splited))
									tmp.Add(node);
							}
						}
						ChList = tmp;
						na = true;
					} else {
						List<HtmlDom> tmp = new List<HtmlDom>();
						foreach (HtmlDom node in ChList) {
							if (val.Equals(node.TagName)) {
								tmp.Add(node);
							}
						}
						ChList = tmp;
						na = true;

					}
				}
				this.combineNodeList(ChList, list);
			} else {
				return null;
			}

			return list;
		}

		public List<HtmlDom> getNodeList() {
			List<HtmlDom> ret = new List<HtmlDom>();
			foreach (HtmlDom node in this.children) {
				ret.Add(node);
			}
			if (this.HasChild()) {
				List<HtmlDom> ret2 = new List<HtmlDom>();
				foreach (HtmlDom node in this.children) {
					List<HtmlDom> child_ret = node.getNodeList();
					foreach (HtmlDom cnode in child_ret) {
						ret2.Add(cnode);
					}
				}
				foreach (HtmlDom node in ret2) {
					ret.Add(node);
				}
			}
			return ret;
		}

		public override string ToString() {
			string ret = "";
			if (this.Type == TagType.SingleTag) {
				ret += "<";
				ret += TagName;
				foreach (KeyValuePair<string, string> attr in this.Attributes) {
					ret += " " + attr.Key + "=\"" + attr.Value + "\"";
				}
				ret += " />\r\n";
			}
			if (this.Type == TagType.BlockTag) {
				ret += "<";
				ret += TagName;
				foreach (KeyValuePair<string, string> attr in this.Attributes) {
					ret += " " + attr.Key + "=\"" + attr.Value + "\"";
				}
				ret += ">\r\n";
				if (this.HasChild())
					foreach (HtmlDom node in this.children)
						ret += node;
				ret += "</";
				ret += TagName;
				ret += ">\r\n";
			}
			if (this.Type == TagType.Text) {
				ret = this.Text;
			}

			return ret;
		}

		private void combineNodeList(List<HtmlDom> from, List<HtmlDom> into) {
			List<int> ignoreIdx = new List<int>();
			foreach (HtmlDom node in into) {
				int listIdx = 0;
				foreach (HtmlDom node2 in from) {
					if (Object.ReferenceEquals(node, node2))
						ignoreIdx.Add(listIdx);
					listIdx++;
				}
			}

			int inputIdx = 0;
			foreach (HtmlDom node in from) {
				if (!ignoreIdx.Contains(inputIdx))
					into.Add(node);
				inputIdx++;
			}
		}
	}
}
