﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Haron.Parser {
	public class OutOfNodeDepthException : HtmlDomException {

		public const string DefaultMessage = "파싱하는 HTML문서에서 1개 이상의 잘못닫힌 블록태그가 있습니다.";

		public OutOfNodeDepthException() : base() {}

		public OutOfNodeDepthException(string message) : base(message) {}

		public OutOfNodeDepthException(string message, int line) : base(message, line) { }

		public OutOfNodeDepthException(string message, string html) : base(message, html) { }

		public OutOfNodeDepthException(string message, string html, int line) : base(message, html, line) { }
	}
}
