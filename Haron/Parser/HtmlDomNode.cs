﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Haron.Parser {

	class DomNodeType {
		public const int NODETYPE_STAG = 1;
		public const int NODETYPE_BTAG = 2;
		public const int NODETYPE_EOBT = 3;
		public const int NODETYPE_ROOT = 4;
		public const int NODETYPE_TEXT = 5;
		public const int NODETYPE_UNKNOWN = 6;
	}

	/*
	 TODO foreach문 모두 for문으로 교체
	 */

	public class HtmlDomNode {
		public int nodeType;
		public string nodeName;	
		public int nodeDepth;
		public Dictionary<string, string> attributes = new Dictionary<string, string>();
		public HtmlDomNode parent = null;
		public List<HtmlDomNode> children = new List<HtmlDomNode>();
		public HtmlDomObject dom;

		public int[] pos = new int[2];
		public int line;
		public string text;
		public string tag;

		public HtmlDomNode(HtmlDomObject origDOM, string name, int type, int line) {
			this.dom = origDOM;
			this.nodeName = name;
			this.nodeType = type;
			this.line = line;
		}
		public HtmlDomNode(HtmlDomObject origDOM, string text) {
			this.dom = origDOM;
			this.text = text;
			this.nodeType = DomNodeType.NODETYPE_TEXT;
	}

			public bool hasParent() {
			return (this.parent != null);
		}

		public bool hasChild() {
			return (this.children.Count > 0);
		}

		public HtmlDomNode firstChild() {
			if (!this.hasChild())
				return null;
			return this.children[0];
		}

		public HtmlDomNode lastChild() {
			if(!this.hasChild())
				return null;
			return this.children[this.children.Count - 1];
		}

		public HtmlDomNode prevSibling() {
			return (this.parent.IndexOf(this) == 0) ? this : this.parent.children[this.parent.IndexOf(this) - 1];
		}

		public HtmlDomNode nextSibling() {
			return (this.parent.IndexOf(this) == this.parent.children.Count - 1) ? this : this.parent.children[this.parent.IndexOf(this) + 1];
		}

		public HtmlDomNode firstSibling() {
			return this.parent.children[0];
		}

		public HtmlDomNode lastSibling() {
			return this.parent.children[this.parent.children.Count - 1];
		}

		public int IndexOf(HtmlDomNode findingNode) {
			if (!this.hasChild())
				return -1;
			else {
				int returnIndex = 0;
				bool still_finding = true;
				do {
					if (returnIndex == this.children.Count) {
						still_finding = false;
						returnIndex = -1;
					} else if (Object.ReferenceEquals(this.children[returnIndex], findingNode)) {
						still_finding = false;
					} else {
						returnIndex++;
					}
				} while (still_finding);

				return returnIndex;
			}
		}

		public HtmlDomNode prepend(HtmlDomNode node) {
			if(node.hasParent())
				node.parent.remove(node);
			node.parent = this;
			this.children.Insert(0, node);
			return node;
		}

		public HtmlDomNode prependTo(HtmlDomNode node) {
			if(this.hasParent())
				this.parent.remove(this);
			this.parent = node;
			node.children.Insert(0, this);
			return this;
		}

		public HtmlDomNode append(HtmlDomNode node) {
			if(node.hasParent())
				node.parent.remove(node);
			node.parent = this;
			this.children.Add(node);
			return node;
		}

		public HtmlDomNode appendTo(HtmlDomNode node) {
			if(this.hasParent())
				this.parent.remove(this);
			this.parent = node;
			node.children.Add(this);
			return this;
		}

		public HtmlDomNode remove(HtmlDomNode node) {
			int i = this.IndexOf(node);
			if(i > -1) {
				this.children[i].parent = null;
				this.children.RemoveAt(i);
			}
			return this;
		}

		public HtmlDomNode getElementById(string id) {
			return this.find("#" + id)[0];
		}

		public HtmlDomNode[] getElementsById(string id) {
			return this.find("#" + id);
		}

		public HtmlDomNode[] getElementsByClassName(string classname) {
			return this.find("." + classname);
		}

		public HtmlDomNode[] getElementsByTagName(string nodeName) {
			return this.find(nodeName);
		}

		public string getAttribute(string name) {
			return this.attributes[name];
		}

		public void setAttribute(string name, string value) {
			if(this.attributes.ContainsKey(name)) {
				this.attributes[name] = value;
			}
			else {
				this.attributes.Add(name, value);
			}
		}

		public string html() {
			return this.text.Replace(this.tag, "").Replace("</" + this.nodeName + ">", "");
		}
		public void html(string text) {
			SetText(text);
		}

		public void AppendText(string text) {
			(new HtmlDomNode(dom, text)).appendTo(this);
		}
		public void PrependText(string text) {
			(new HtmlDomNode(dom, text)).prependTo(this);
		}
		public void SetText(string text) {
			foreach(HtmlDomNode r in children) {
				remove(r);
			}
			AppendText(text);
		}
		/**
		
		*/

		public HtmlDomNode[] find(string selector) {
			List<HtmlDomNode> list = new List<HtmlDomNode>();

			if(this.hasChild()) {
				Regex RegularExp = new Regex(@"\[[^\]]*\]|\*|[#.\w]?[\w]*\w|[,>+~\s]*[,>+~\s]");
				MatchCollection matches = RegularExp.Matches(selector);
				List<HtmlDomNode> ChList = this.getNodeList();
				bool na = false;

				for(int i = 0; i <= matches.Count - 1; i++) {
					Match match = matches[i];
					string val = match.Value;

					if(val.Contains(".")) {
						List<HtmlDomNode> found = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							if(node.attributes.ContainsKey("class")) {
								string[] splited = node.attributes["class"].Split(' ');
								if(splited.ToList<string>().Contains(val.Substring(1))) {
									found.Add(node);
								}
							}
						}
						ChList = found;
						na = true;
					}
					else if(val.Contains("#")) {
						List<HtmlDomNode> found = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							if(node.attributes.ContainsKey("id")) {
								if(node.attributes["id"].Equals(val.Substring(1))) {
									found.Add(node);
								}
							}
						}
						ChList = found;
						na = true;
					}
					else if(val.Equals('*')) {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							foreach(HtmlDomNode node2 in node.getNodeList()) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = true;
					}
					else if(val.Contains(",") && na) {
						//쉼표 구분한 순서대로 리스트에 추가하는 버그
						this.combineNodeList(ChList, list);
						ChList = this.getNodeList();
						na = false;
					}
					else if(val.Equals(" ") && na) {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							foreach(HtmlDomNode node2 in node.getNodeList()) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = false;
					}
					else if(val.Contains(">") && na) {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							foreach(HtmlDomNode node2 in node.children) {
								tmp.Add(node2);
							}
						}
						ChList = tmp;
						na = false;
					}
					else if(val.Contains("+") && na) {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							tmp.Add(node.nextSibling());
						}
						ChList = tmp;
						na = false;
					}
					else if(val.Contains("~") && na) {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							HtmlDomNode node2 = node.nextSibling();
							while(!Object.ReferenceEquals(node2, node2.lastSibling())) {
								tmp.Add(node2);
								node2 = node2.nextSibling();
							}
							tmp.Add(node2.nextSibling());
						}
						ChList = tmp;
						na = false;
					}
					else if(val[0] == '[' && val[val.Length - 1] == ']') {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						if(val.IndexOf("~=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "~=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key)) {
									string[] strarr = node.attributes[key].Split(new char[] { ' ' });
									foreach(string str in strarr)
										if(str.Equals(value))
											tmp.Add(node);
								}
							}
						}
						else if(val.IndexOf("|=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "|=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key)) {
									string attrval = node.attributes[key];
									if((attrval.IndexOf("-" + value) > -1) || (attrval.IndexOf(value + "-") > -1) || (attrval.IndexOf("-" + value + "-") > -1))
										tmp.Add(node);
								}
							}
						}
						else if(val.IndexOf("^=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "^=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key)) {
									string attrval = node.attributes[key];
									string start = attrval.Substring(0, value.Length);
									if(start.Equals(value))
										tmp.Add(node);
								}
							}
						}
						else if(val.IndexOf("$=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "$=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key)) {
									string attrval = node.attributes[key];
									string end = attrval.Substring(attrval.Length - value.Length);
									if(end.Equals(value))
										tmp.Add(node);
								}
							}
						}
						else if(val.IndexOf("*=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "*=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key))
									if(node.attributes[key].IndexOf(value) > -1)
										tmp.Add(node);
							}
						}
						else if(val.IndexOf("=") > -1) {
							string[] splited = val.Substring(1, val.Length - 2).Split(new string[] { "=" }, StringSplitOptions.None);
							string key = splited[0];
							string value = splited[1];
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(key))
									if(node.attributes[key] == value)
										tmp.Add(node);
							}
						}
						else {
							string splited = val.Substring(1, val.Length - 2);
							foreach(HtmlDomNode node in ChList) {
								if(node.attributes.ContainsKey(splited))
									tmp.Add(node);
							}
						}
						ChList = tmp;
						na = true;
					}
					else {
						List<HtmlDomNode> tmp = new List<HtmlDomNode>();
						foreach(HtmlDomNode node in ChList) {
							if(val.Equals(node.nodeName)) {
								tmp.Add(node);
							}
						}
						ChList = tmp;
						na = true;

					}
				}
				this.combineNodeList(ChList, list);
			}
			else {
				return null;
			}

			return list.ToArray();
		}

		public List<HtmlDomNode> getNodeList() {
			List<HtmlDomNode> ret = new List<HtmlDomNode>();
			foreach(HtmlDomNode node in this.children) {
				ret.Add(node);
			}
			if(this.hasChild()) {
				List<HtmlDomNode> ret2 = new List<HtmlDomNode>();
				foreach(HtmlDomNode node in this.children) {
					List<HtmlDomNode> child_ret = node.getNodeList();
					foreach(HtmlDomNode cnode in child_ret) {
						ret2.Add(cnode);
					}
				}
				foreach(HtmlDomNode node in ret2) {
					ret.Add(node);
				}
			}
			return ret;
		}

		public List<HtmlDomNode> getAllNodeList() {
			return this.dom.NodeList;
		}

		public override string ToString() {
			string ret = "";
			if(this.nodeType == DomNodeType.NODETYPE_STAG) {
				ret += "<";
				ret += nodeName;
				foreach (KeyValuePair<string, string> attr in this.attributes) {
					ret += " " + attr.Key + "=\"" + attr.Value + "\"";
				}
				ret += " />";
			}
			if (this.nodeType == DomNodeType.NODETYPE_BTAG) {
				ret += "<";
				ret += nodeName;
				foreach (KeyValuePair<string, string> attr in this.attributes) {
					ret += " " + attr.Key + "=\"" + attr.Value + "\"";
				}
				ret += ">";
				if (this.hasChild()) 
					foreach(HtmlDomNode node in this.children) 
						ret += node;
				ret += "</";
				ret += nodeName;
				ret += ">";
			}
			if (this.nodeType == DomNodeType.NODETYPE_TEXT) {
				ret = this.text;
			}


			return ret;
		}

		public string toString() {
			return this.text;
		}

		private void combineNodeList(List<HtmlDomNode> from, List<HtmlDomNode> into) {
			List<int> ignoreIdx = new List<int>();
			foreach(HtmlDomNode node in into) {
				int listIdx = 0;
				foreach(HtmlDomNode node2 in from) {
					if(Object.ReferenceEquals(node, node2))
						ignoreIdx.Add(listIdx);
					listIdx++;
				}
			}

			int inputIdx = 0;
			foreach(HtmlDomNode node in from) {
				if(!ignoreIdx.Contains(inputIdx))
					into.Add(node);
				inputIdx++;
			}
		}
	}
}
