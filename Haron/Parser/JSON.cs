﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net.Http;

namespace Haron.Parser {
	public enum JsonObjectType {
		@null = 0,
		@string,
		number,
		array,
		@object,
		boolean
	}

	public class JSON {
		//const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"[^,\\n{}\\[\\]:]*\"+:*";
		const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"(\\\\\"|[^\"])*\":*";
		const string ESCAPED_UNICODE_PATTERN = @"\\[uU]([0-9A-Fa-f]{4})";

		public JsonObjectType Type;
		public object Value = null;
		public JSON Parent;

		public bool IsNull { get { return this.Type == JsonObjectType.@null; } set { } }

		public static JSON Parse(string json) {
			Regex regexr = new Regex(REGEX_PATTERN);
			MatchCollection splited = regexr.Matches(json);

			if(splited.Count == 0) {
				throw new Exception("JSON형식의 요소를 발견할 수 없었습니다.");
			}

			int current_depth = 0;
			JSON current_object = new JSON();
			JSON RootObject = current_object;
			//this.RootObject.Parent = this.RootObject;

			foreach(Match piece in splited) {
				string c = piece.Value;
				int i = piece.Index;
				double f = 0;
				if(c == "{") {
					//node adder & object start
					current_depth++;
					if(current_object.Type == JsonObjectType.array) {
						JSON new_object = new JSON(new Dictionary<string, JSON>(), JsonObjectType.@object);
						JSON parent = current_object;
						new_object.Parent = parent;
						((List<JSON>)current_object.Value).Add(new_object);
						current_object = new_object;
					}
					else {
						current_object.Type = JsonObjectType.@object;
						current_object.Value = (object)(new Dictionary<string, JSON>());
					}
				}
				else if(c == "}") {
					//node subtractor & end of object
					current_depth--;
					current_object = current_object.Parent;
				}
				else if(c == "[") {
					//node adder & array start
					current_depth++;
					if(current_object.Type == JsonObjectType.array) {
						JSON new_object = new JSON(new List<JSON>(), JsonObjectType.array);
						JSON parent = current_object;
						new_object.Parent = parent;
						((List<JSON>)current_object.Value).Add(new_object);
						current_object = new_object;
					}
					else {
						current_object.Type = JsonObjectType.array;
						current_object.Value = (object)(new List<JSON>());
					}
				}
				else if(c == "]") {
					//node substrator & end of array
					current_depth--;
					current_object = current_object.Parent;
				}
				else if(c == ",") {
					//divider

				}
				else if(c[c.Length - 1] == ':') {
					//object initialize
					string name = Unescape(c.Substring(1, c.LastIndexOf('\"') - 1));
					JSON new_object = new JSON();
					JSON parent = current_object;
					new_object.Parent = parent;
					if(current_object.Type == JsonObjectType.@object) {
						((Dictionary<string, JSON>)current_object.Value).Add(name, new_object);
					}
					else if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new_object);
					}
					current_object = new_object;
				}

				else if(double.TryParse(c, out f)) {
					//number(double)
					if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new JSON((object)f, JsonObjectType.number));
					}
					else {
						current_object.Type = JsonObjectType.number;
						current_object.Value = (object)f;
						current_object = current_object.Parent;
					}
				}
				else if(c == "true") {
					//boolean of true
					if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new JSON((object)true, JsonObjectType.boolean));
					}
					else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)true;
						current_object = current_object.Parent;
					}
				}
				else if(c == "false") {
					//boolean of false
					if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new JSON((object)false, JsonObjectType.boolean));
					}
					else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)false;
						current_object = current_object.Parent;
					}
				}
				else if(c == "null") {
					//null object
					if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new JSON((object)null, JsonObjectType.@null));
					}
					else {
						current_object.Type = JsonObjectType.@null;
						current_object.Value = (object)null;
						current_object = current_object.Parent;
					}
				}
				else if(c[0] == '\"' && c[c.Length - 1] == '\"') {
					//general string
					string strdata = Unescape(c.Trim(new char[] { '"' }));
					if(current_object.Type == JsonObjectType.array) {
						((List<JSON>)current_object.Value).Add(new JSON((object)strdata, JsonObjectType.@string));
					}
					else {
						current_object.Type = JsonObjectType.@string;
						current_object.Value = (object)strdata;
						current_object = current_object.Parent;
					}
				}
			}

			return RootObject;
		}

		public static string Unescape(string str) {
			string tmp = str
			.Replace("\\\\", ((char)0x5C).ToString())
			.Replace("\\\"", ((char)0x22).ToString())
			.Replace("\\/", ((char)0x2F).ToString())
			.Replace("\\b", ((char)0x08).ToString())
			.Replace("\\f", ((char)0x0C).ToString())
			.Replace("\\n", ((char)0x0A).ToString())
			.Replace("\\r", ((char)0x0D).ToString())
			.Replace("\\t", ((char)0x09).ToString());
			string ret = tmp;
			Regex unicode_extract = new Regex(ESCAPED_UNICODE_PATTERN);
			Match match = unicode_extract.Match(ret);
			while(match.Success) {
				string rep = ret.Substring(0, match.Index) + ((char)Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString() + ret.Substring(match.Index + match.Length);
				ret = rep;
				match = unicode_extract.Match(ret);
			}
			return ret;
		}

		public static int[] ESCAPE_CONTROL_CHAR = {
			0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
			0x0B, 0x0C, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14,
			0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,
			0x1E, 0x1F, 0x7F, 0x80, 0x81, 0x82, 0x83, 0x84, 0x86,
			0x87, 0x88, 0x89, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95,
			0x96, 0x98, 0x99, 0x9A, 0x9B, 0x9C, 0x9D, 0x9E, 0x9F
		};

		public static string Escape(string str) {
			string tmp = str
			.Replace(((char)0x5C).ToString(), "\\\\")
			.Replace(((char)0x22).ToString(), "\\\"")
			.Replace(((char)0x2F).ToString(), "\\/")
			.Replace(((char)0x08).ToString(), "\\b")
			.Replace(((char)0x0C).ToString(), "\\f")
			.Replace(((char)0x0A).ToString(), "\\n")
			.Replace(((char)0x0D).ToString(), "\\r")
			.Replace(((char)0x09).ToString(), "\\t");
			string ret = tmp;
			for(int i = 0; i < tmp.Length; ++i) {
				bool found = false;
				for(int j = 0; j < ESCAPE_CONTROL_CHAR.Length && !found; ++j)
					found = (int)tmp[i] == ESCAPE_CONTROL_CHAR[j];
				if(tmp[i] > 0xFF || found) {
					byte[] b = Encoding.Unicode.GetBytes(tmp[i].ToString());
					ret = ret.Replace(tmp[i].ToString(), String.Format("\\u{0:X2}{1:X2}", (int)b[1], (int)b[0]));
				}
			}
			return ret;
		}

		public JSON this[int index] {
			get {
				if(this.Type == JsonObjectType.array) {
					return ((List<JSON>)this.Value)[index];
				}
				else if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JSON>)this.Value)[index.ToString()];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					List<JSON> tmp = (List<JSON>)this.Value;
					if(tmp.Count > index)
						tmp[index] = value;
					else
						for(int i = index; i > tmp.Count - 1; --i)
							if(i == index)
								tmp.Add(value);
							else
								tmp.Add(new JSON(null));
					this.Value = (object)tmp;
				}
				else if(this.Type == JsonObjectType.@object) {
					this[index.ToString()] = value;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JSON this[string key] {
			get {
				if(this.Type == JsonObjectType.array) {
					try {
						return ((List<JSON>)this.Value)[int.Parse(key)];
					}
					catch(FormatException e) {
						throw new IndexOutOfRangeException("", e);
					}
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JSON> obj = (Dictionary<string, JSON>)this.Value;
					if(!obj.ContainsKey(key)) return null;
					return (obj)[key];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					this[int.Parse(key)] = value;
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JSON> tmp = (Dictionary<string, JSON>)this.Value;
					if(tmp.ContainsKey(key))
						tmp[key] = value;
					else
						tmp.Add(key, value);

					this.Value = (object)tmp;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JSON() {
			this.Type = JsonObjectType.@object;
			this.Value = new Dictionary<string, JSON>();
		}
		public JSON(object value) {
			if(value is string) {
				this.Type = JsonObjectType.@string;
			}
			else if(value is sbyte || value is byte || value is short || value is ushort || value is int || value is uint || value is long || value is ulong || value is float || value is double || value is decimal) {
				this.Type = JsonObjectType.number;
			}
			else if(value is bool) {
				this.Type = JsonObjectType.boolean;
			}
			else if(value is null) {
				this.Type = JsonObjectType.@null;
			}
			else if(value is Array) {
				this.Type = JsonObjectType.array;
				this.Value = new List<JSON>();
				Array arr = (Array)value;
				for(int i = 0; i < arr.Length; ++i) {
					this[i] = new JSON(arr.GetValue(i));
				}
			}
			else {
				throw new FormatException();
			}
			if(this.Value is null) {
				this.Value = value;
			}
		}

		public JSON(JsonObjectType type) {
			this.Type = type;
			switch(this.Type) {
				case JsonObjectType.array:
					this.Value = new List<JSON>();
					break;
				case JsonObjectType.@object:
					this.Value = new Dictionary<string, JSON>();
					break;
				default:
					throw new FormatException();
			}
		}

		public JSON(object value, JsonObjectType type) {
			this.Value = value;
			this.Type = type;
		}

		public Dictionary<string, JSON> ToDictionary() {
			if(this.Type == JsonObjectType.@object) {
				return (Dictionary<string, JSON>)this.Value;
			}
			else {
				throw new Exception("Dictionary로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public bool ToBoolean() {
			if(this.Type == JsonObjectType.boolean) {
				return (bool)this.Value;
			}
			else {
				throw new Exception("string으로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public override string ToString() {
			string ret = "";
			if(this.Type == JsonObjectType.boolean) {
				ret = this.ToBoolean().ToString().ToLower();
			}
			else if(this.Type == JsonObjectType.@null) {
				ret = "null";
			}
			else if(this.Type == JsonObjectType.number) {
				ret = this.Value.ToString();
			}
			else if(this.Type == JsonObjectType.@string) {
				ret = Unescape((string)this.Value);
			}
			else {
				throw new Exception("string으로 변환 가능한 JsonObject가 아닙니다.");
			}
			return ret;
		}

		public static string Stringify(JSON json) {
			string ret;
			if(json.Type == JsonObjectType.@string) {
				ret = Escape((string)json.Value);
			}
			else if(json.Type == JsonObjectType.array) {
				ret = "[";
				JSON[] array = json.ToArray();
				for(int i = 0; i < array.Length; ++i) {
					if(array[i].Value == null) {
						ret += "null";
					}
					else if(array[i].Value is string) {
						ret += "\"" + Stringify(array[i]) + "\"";
					}
					else if(array[i].Value is null) {
						ret += "null";
					}
					else {
						ret += Stringify(array[i]);
					}
					if(i != array.Length - 1) {
						ret += ",";
					}
				}
				ret += "]";
			}
			else if(json.Type == JsonObjectType.@object) {
				ret = "{";
				Dictionary<string, JSON> dic = json.ToDictionary();
				int i = 0;
				foreach(KeyValuePair<string, JSON> pair in dic) {
					ret += "\"" + pair.Key + "\":";
					if(pair.Value.Value is string) {
						ret += "\"" + Stringify(pair.Value) + "\"";
					}
					else if(pair.Value.Value is null) {
						ret += "null";
					}
					else {
						ret += Stringify(pair.Value);
					}
					if(i != dic.Count - 1) {
						ret += ",";
					}
					++i;
				}
				ret += "}";
			}
			else {
				ret = json.ToString();
			}

			return ret;
		}

		public double ToDouble() {
			if(this.Type == JsonObjectType.number) {
				return (double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int ToInt() {
			if(this.Type == JsonObjectType.number) {
				return (int)(double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public JSON[] ToArray() {
			return this.ToList().ToArray();
		}

		public List<JSON> ToList() {
			if(this.Type == JsonObjectType.array) {
				return (List<JSON>)this.Value;
			}
			else {
				throw new Exception("List로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int[] ToIntArray() {
			JSON[] arr = this.ToArray();
			int[] ret = new int[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToInt();
			}
			return ret;
		}

		public double[] ToDoubleArray() {
			JSON[] arr = this.ToArray();
			double[] ret = new double[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToDouble();
			}
			return ret;
		}

		public string[] ToStringArray() {
			JSON[] arr = this.ToArray();
			string[] ret = new string[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToString();
			}
			return ret;
		}

		public bool KeyExists(int key) {
			return ToList().Count - 1 >= key;
		}

		public bool KeyExists(string key) {
			return ToDictionary().ContainsKey(key);
		}

		public void Add(JSON j) {
			List<JSON> tmp = (List<JSON>)this.Value;
			tmp.Add(j);
		}

		public int Count {
			get {
				if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JSON>)this.Value).Count;
				}
				else if(this.Type == JsonObjectType.array) {
					return ((List<JSON>)this.Value).Count;
				}
				else {
					throw new FormatException();
				}
			}
		}

		public DateTime ToDateTime() {
			if(this.Type == JsonObjectType.@string) {
				return DateTime.Parse(this.ToString(), null, DateTimeStyles.RoundtripKind);
			}
			else {
				throw new FormatException();
			}
		}

		public DateTimeOffset ToDateTimeOffset() {
			if(this.Type == JsonObjectType.number) {
				return DateTimeOffset.FromUnixTimeMilliseconds((long)this.ToDouble());
			}
			else {
				throw new FormatException();
			}
		}

		public static JSON FromTime(DateTimeOffset off) {
			return new JSON((double)off.ToUnixTimeMilliseconds(), JsonObjectType.number);
		}

		public static JSON FromTime(DateTime time) {
			return new JSON(time.ToString("o"), JsonObjectType.@string);
		}

		public static implicit operator JSON(sbyte v) { return new JSON(v); }
		public static explicit operator sbyte(JSON v) { return (sbyte)v.Value; }
		public static implicit operator JSON(byte v) { return new JSON(v); }
		public static explicit operator byte(JSON v) { return (byte)v.Value; }
		public static implicit operator JSON(short v) { return new JSON(v); }
		public static explicit operator short(JSON v) { return (short)v.Value; }
		public static implicit operator JSON(ushort v) { return new JSON(v); }
		public static explicit operator ushort(JSON v) { return (ushort)v.Value; }
		public static implicit operator JSON(int v) { return new JSON(v); }
		public static explicit operator int(JSON v) { return (int)v.Value; }
		public static implicit operator JSON(uint v) { return new JSON(v); }
		public static explicit operator uint(JSON v) { return (uint)v.Value; }
		public static implicit operator JSON(long v) { return new JSON(v); }
		public static explicit operator long(JSON v) { return (long)v.Value; }
		public static implicit operator JSON(ulong v) { return new JSON(v); }
		public static explicit operator ulong(JSON v) { return (ulong)v.Value; }
		public static implicit operator JSON(decimal v) { return new JSON(v); }
		public static explicit operator decimal(JSON v) { return (decimal)v.Value; }
		public static implicit operator JSON(float v) { return new JSON(v); }
		public static explicit operator float(JSON v) { return (float)v.Value; }
		public static implicit operator JSON(double v) { return new JSON(v); }
		public static explicit operator double(JSON v) { return (double)v.Value; }
		public static implicit operator JSON(string v) { return new JSON(v); }
		public static explicit operator string(JSON v) { return (string)v.Value; }
		public static implicit operator JSON(bool v) { return new JSON(v); }
		public static explicit operator bool(JSON v) { return (bool)v.Value; }
		public static implicit operator JSON(DateTime v) { return JSON.FromTime(v); }
		public static explicit operator DateTime(JSON v) { return v.ToDateTime(); }
		public static implicit operator JSON(DateTimeOffset v) { return JSON.FromTime(v); }
		public static explicit operator DateTimeOffset(JSON v) { return v.ToDateTimeOffset(); }
	}
}
