﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Haron.Parser {
	public class HtmlDomException : Exception {
		public string html;
		public int line;

		public HtmlDomException() : base() {}

		public HtmlDomException(string message) : base(message) {}

		public HtmlDomException(string message, int line) : base(message) {
			this.line = line;
		}

		public HtmlDomException(string message, string html) : base(message) {
			this.html = html;
		}

		public HtmlDomException(string message, string html, int line) : base(message) {
			this.html = html;
			this.line = line;
		}
	}
}
