﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Haron.Parser {
	public enum JsonObjectType {
		@string,
		number,
		array,
		@object,
		boolean,
		@null
	}

	public class JsonDomObject {
		//const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"[^,\\n{}\\[\\]:]*\"+:*";
		const string REGEX_PATTERN = "{|}|\\[|\\]|true|false|null|,|-*[\\d.]+|\"(\\\\\"|[^\"])*\":*";
		const string ESCAPED_UNICODE_PATTERN = @"\\[uU]([0-9A-Fa-f]{4})";

		public JsonObjectType Type;
		public string Name;
		public object Value;
		public JsonDomObject Parent;

		public bool IsNull { get { return this.Type == JsonObjectType.@null; } set { } }
		
		public static JsonDomObject Parse(string json) {
			Regex regexr = new Regex(REGEX_PATTERN);
			MatchCollection splited = regexr.Matches(json);

			if (splited.Count == 0) {
				throw new Exception("JSON형식의 요소를 발견할 수 없었습니다.");
			}

			int current_depth = 0;
			JsonDomObject current_object = new JsonDomObject();
			JsonDomObject RootObject = current_object;
			//this.RootObject.Parent = this.RootObject;

			foreach (Match piece in splited) {
				string c = piece.Value;
				int i = piece.Index;
				double f = 0;
				if (c == "{") {
					//node adder & object start
					current_depth++;
					if (current_object.Type == JsonObjectType.array) {
						JsonDomObject new_object = new JsonDomObject(new Dictionary<string, JsonDomObject>(), JsonObjectType.@object);
						JsonDomObject parent = current_object;
						new_object.Parent = parent;
						((List<JsonDomObject>)current_object.Value).Add(new_object);
						current_object = new_object;
					} else {
						current_object.Type = JsonObjectType.@object;
						current_object.Value = (object)(new Dictionary<string, JsonDomObject>());
					}
				} else if (c == "}") {
					//node subtractor & end of object
					current_depth--;
					current_object = current_object.Parent;
				} else if (c == "[") {
					//node adder & array start
					current_depth++;
					if (current_object.Type == JsonObjectType.array) {
						JsonDomObject new_object = new JsonDomObject(new List<JsonDomObject>(), JsonObjectType.array);
						JsonDomObject parent = current_object;
						new_object.Parent = parent;
						((List<JsonDomObject>)current_object.Value).Add(new_object);
						current_object = new_object;
					} else {
						current_object.Type = JsonObjectType.array;
						current_object.Value = (object)(new List<JsonDomObject>());
					}
				} else if (c == "]") {
					//node substrator & end of array
					current_depth--;
					current_object = current_object.Parent;
				} else if (c == ",") {
					//divider

				} else if (c[c.Length - 1] == ':') {
					//object initialize
					string name = EscapedUnicodeToString(c.Substring(1, c.LastIndexOf('\"') - 1));
					JsonDomObject new_object = new JsonDomObject(name);
					JsonDomObject parent = current_object;
					new_object.Parent = parent;
					if (current_object.Type == JsonObjectType.@object) {
						((Dictionary<string, JsonDomObject>)current_object.Value).Add(name, new_object);
					} else if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new_object);
					}
					current_object = new_object;
				}


				  /*else if(int.TryParse(c, out i)) {
					  //number(int)
					  current_object.Type = JsonObjectType.number;
					  current_object.Value = (object)i;
					  current_object = current_object.Parent;
				  }*/


				  else if (double.TryParse(c, out f)) {
					//number(double)
					if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new JsonDomObject((object)f, JsonObjectType.number));
					} else {
						current_object.Type = JsonObjectType.number;
						current_object.Value = (object)f;
						current_object = current_object.Parent;
					}
				} else if (c == "true") {
					//boolean of true
					if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new JsonDomObject((object)true, JsonObjectType.boolean));
					} else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)true;
						current_object = current_object.Parent;
					}
				} else if (c == "false") {
					//boolean of false
					if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new JsonDomObject((object)false, JsonObjectType.boolean));
					} else {
						current_object.Type = JsonObjectType.boolean;
						current_object.Value = (object)false;
						current_object = current_object.Parent;
					}
				} else if (c == "null") {
					//null object
					if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new JsonDomObject((object)null, JsonObjectType.@null));
					} else {
						current_object.Type = JsonObjectType.@null;
						current_object.Value = (object)null;
						current_object = current_object.Parent;
					}
				} else if (c[0] == '\"' && c[c.Length - 1] == '\"') {
					//general string
					string strdata = EscapedUnicodeToString(c.Trim(new char[] { '"' }));
					if (current_object.Type == JsonObjectType.array) {
						((List<JsonDomObject>)current_object.Value).Add(new JsonDomObject((object)strdata, JsonObjectType.@string));
					} else {
						current_object.Type = JsonObjectType.@string;
						current_object.Value = (object)strdata;
						current_object = current_object.Parent;
					}
				}
			}

			return RootObject;
		}

		public static string EscapedUnicodeToString(string orig) {
			string ret = orig;
			Regex unicode_extract = new Regex(ESCAPED_UNICODE_PATTERN);
			Match match = unicode_extract.Match(ret);
			while (match.Success) {
				string rep = ret.Substring(0, match.Index) + ((char)Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString() + ret.Substring(match.Index + match.Length);
				ret = rep;
				match = unicode_extract.Match(ret);
			}

			return ret;
		}



		public JsonDomObject this[int index] {
			get {
				if(this.Type == JsonObjectType.array) {
					return ((List<JsonDomObject>)this.Value)[index];
				}
				else if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JsonDomObject>)this.Value)[index.ToString()];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					List<JsonDomObject> tmp = (List<JsonDomObject>)this.Value;
					tmp[index] = value;
					this.Value = (object)tmp;
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JsonDomObject> tmp = (Dictionary<string, JsonDomObject>)this.Value;
					tmp[index.ToString()] = value;
					this.Value = (object)tmp;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JsonDomObject this[string index] {
			get {
				if(this.Type == JsonObjectType.array) {
					try {
						return ((List<JsonDomObject>)this.Value)[int.Parse(index)];
					}
					catch(FormatException e) {
						throw new IndexOutOfRangeException("", e);
					}
				}
				else if(this.Type == JsonObjectType.@object) {
					return ((Dictionary<string, JsonDomObject>)this.Value)[index];
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
			set {
				if(this.Type == JsonObjectType.array) {
					try {
						List<JsonDomObject> tmp = (List<JsonDomObject>)this.Value;
						tmp[int.Parse(index)] = value;
						this.Value = (object)tmp;
					}
					catch(FormatException e) {
						throw new IndexOutOfRangeException("", e);
					}
				}
				else if(this.Type == JsonObjectType.@object) {
					Dictionary<string, JsonDomObject> tmp = (Dictionary<string, JsonDomObject>)this.Value;
					tmp[index] = value;
					this.Value = (object)tmp;
				}
				else {
					throw new IndexOutOfRangeException();
				}
			}
		}

		public JsonDomObject() {
			this.Type = JsonObjectType.@object;
		}

		public JsonDomObject(JsonObjectType type) {
			this.Type = type;
		}

		public JsonDomObject(object value, JsonObjectType type) {
			this.Value = value;
			this.Type = type;
		}

		public JsonDomObject(string name) {
			this.Name = name;
		}

		public JsonDomObject(string name, object value, JsonObjectType type) {
			this.Name = name;
			this.Value = value;
			this.Type = type;
		}

		public Dictionary<string, JsonDomObject> ToDictionary() {
			if(this.Type == JsonObjectType.@object) {
				return (Dictionary<string, JsonDomObject>)this.Value;
			}
			else {
				throw new Exception("Dictionary로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public bool ToBoolean() {
			if(this.Type == JsonObjectType.boolean) {
				return (bool)this.Value;
			}
			else {
				throw new Exception("string으로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		
		public override string ToString() {
			if(this.Type == JsonObjectType.@string) {
				return (string)this.Value;
			}
			else {
				throw new Exception("string으로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public double ToDouble() {
			if(this.Type == JsonObjectType.number) {
				return (double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int ToInt() {
			if(this.Type == JsonObjectType.number) {
				return (int)(double)this.Value;
			}
			else {
				throw new Exception("number로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public JsonDomObject[] ToArray() {
			return this.ToList().ToArray();
		}

		public List<JsonDomObject> ToList() {
			if(this.Type == JsonObjectType.array) {
				return (List<JsonDomObject>)this.Value;
			}
			else {
				throw new Exception("List로 변환 가능한 JsonObject가 아닙니다.");
			}
		}

		public int[] ToIntArray() {
			JsonDomObject[] arr = this.ToArray();
			int[] ret = new int[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToInt();
			}
			return ret;
		}

		public double[] ToDoubleArray() {
			JsonDomObject[] arr = this.ToArray();
			double[] ret = new double[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToDouble();
			}
			return ret;
		}

		public string[] ToStringArray() {
			JsonDomObject[] arr = this.ToArray();
			string[] ret = new string[arr.Length];
			for(int i = 0; i < ret.Length; i++) {
				ret[i] = arr[i].ToString();
			}
			return ret;
		}

		public bool KeyExists(int key) {
			return ToList().Count - 1 >= key;
		}

		public bool KeyExists(string key) {
			return ToDictionary().ContainsKey(key);
		}
	}
}
