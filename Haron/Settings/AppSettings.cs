﻿using Haron.Log;
using Haron.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Settings
{
	public class AppSettings {
		private static AppSettings _INSTANCE = null;
		public static AppSettings Current {
			get {
				if (_INSTANCE == null) _INSTANCE = new AppSettings();
				return _INSTANCE;
			}
		}

		private JSON Parsed;

		public string ServiceName {
			get { return this.Parsed["ServiceSetting"]["ServiceName"].ToString(); }
			set { this.Parsed["ServiceSetting"]["ServiceName"] = value; }
		}
		public string DisplayName {
			get { return this.Parsed["ServiceSetting"]["DisplayName"].ToString(); }
			set { this.Parsed["ServiceSetting"]["DisplayName"] = value; }
		}
		public string Description {
			get { return this.Parsed["ServiceSetting"]["Description"].ToString(); }
			set { this.Parsed["ServiceSetting"]["Description"] = value; }
		}
		public string PluginPath {
			get { return this.Parsed["Plugin"]["DllPath"].ToString(); }
			set { this.Parsed["Plugin"]["DllPath"] = value; }
		}
		public JSON this[string key] {
			get { return this.Parsed[key]; }
			set { this.Parsed[key] = value; }
		}

		private void LoadDefault() {
			this.Parsed = new JSON();
			this.Parsed["ServiceSetting"] = new JSON();
			this.Parsed["Plugin"] = new JSON();

			this.ServiceName = "HaronSvc";
			this.DisplayName = "Haron Service";
			this.Description = "Service for Haron server";
			this.PluginPath = "./plugins";
		}

		private AppSettings() {
			if (File.Exists(Values.HARON_JSON_FILE)) {
				try { 
					this.Parsed = JSON.Parse(File.ReadAllText(Values.HARON_JSON_FILE));
					return;
				}
				catch(Exception) {
					LogStream.Log(Values.HARON_JSON_FILE + " has been corrupted. load and save default settings.", true);
				}
			}
			this.LoadDefault();
			this.Parsed = this.Save();
		}

		public JSON Save() {
			File.WriteAllText(Values.HARON_JSON_FILE, JSON.Stringify(this.Parsed));
			return this.Parsed;
		}
	}
}
