﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Settings {
	public static class Values {
		public static bool ConsoleInterfaceEnabled = false;
		public static bool LoadThirdPartyPlugins = true;
		public static bool TestRun = false;
		public static bool UseWindowsForm = false;
		public const string HARON_JSON_FILE = "haron.json";
	}
}
