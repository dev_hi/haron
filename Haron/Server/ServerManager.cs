﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Server {
	public static class ServerManager {
		private static List<IServer> _CreatedServer = new List<IServer>();
		private static List<Type> _CreatableServer = new List<Type>(Assembly.GetExecutingAssembly().GetTypes().Where<Type>(new Func<Type, bool>(_IServerFilter)));

		internal static void AddCreatableServer(Type[] types) {
			_CreatableServer.AddRange(types.Where<Type>(new Func<Type, bool>(_IServerFilter)));
		}

		private static bool _IServerFilter(Type t) {
			return t.GetInterface(typeof(IServer).FullName) != null;
		}

		public static Type GetCreatableType(Type search = null) {
			return GetCreatableType(search.FullName);
		}

		public static Type GetCreatableType(string name = null) {
			for(int i = 0; i < _CreatableServer.Count; ++i) {
				if(_CreatableServer[i].FullName.Equals(name)) return _CreatableServer[i];
			}
			return null;
		}

		public static T CreateServer<T>() {
			Type templateType = typeof(T);
			T ret = (T)Activator.CreateInstance(templateType);
			RegisterServer((IServer)ret);
			return ret;
			/*if(_CreatableServer.Contains(templateType)) {
			}
			return ;*/
		}

		public static void RegisterServer(IServer server) {
			_CreatedServer.Add(server);
		}

		public static T[] GetServer<T>() {
			Type t = typeof(T);
			List<T> ret = new List<T>();
			for(int i =0; i < _CreatedServer.Count; ++i) {
				if(_CreatedServer[i].GetType().Equals(t)) ret.Add((T)_CreatedServer[i]);
			}
			return ret.ToArray();
		}
	}
}
