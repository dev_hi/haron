﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Haron.Server {
	public class SocketPool {
		public int MaxSocket {
			get {
				return this.SocketList.Length;
			}
		}
		private Queue<Socket> SocketQueue = new Queue<System.Net.Sockets.Socket>();
		private Socket[] SocketList;
		public SocketPool(int max) {
			this.SocketList = new Socket[max];
		}

		public Socket Enqueue(Socket socket) {
			this.SocketQueue.Enqueue(socket);
			System.Net.Sockets.Socket ret = null;
			while (ret == null) {
				for (int i = 0; i < this.SocketList.Length; ++i) {
					if (this.SocketList[i] == null) {
						ret = this.SocketList[i] = this.SocketQueue.Dequeue();
						break;
					}
					else if (!this.SocketList[i].Connected) {
						ret = this.SocketList[i] = this.SocketQueue.Dequeue();
						break;
					}
				}
			}
			return ret;
		}

		public int GetSocketCount() {
			int ret = 0;
			for (int i = 0; i < this.SocketList.Length; ++i) {
				if (this.SocketList[i] != null) if (this.SocketList[i].Connected) ++ret;
			}
			return ret;
		}

		public void Destroy() {
			while (this.SocketQueue.Count > 0) {
				System.Net.Sockets.Socket s = this.SocketQueue.Dequeue();
				if(s.Connected) s.Disconnect(false);
			}
			for (int i = 0; i < this.SocketList.Length; ++i) {
				if (this.SocketList[i] != null) {
					if (this.SocketList[i].Connected) {
						this.SocketList[i].Disconnect(false);
					}
				}
			}
		}

	}
}
