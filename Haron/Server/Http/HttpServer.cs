﻿using Haron.Server.Http.MiddleWare;
using Haron.Server.Http.Packet;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Haron.Server.Http {
	public delegate void OnRequestCallback(HttpRequest req);
	public delegate void OnResponseCallback(HttpRequest req, HttpResponse res);
	public class HttpServer : IServer {
		public IPAddress BindAddress {
			get {
				return this._transportServer.BindAddress;
			}
			set {
				this._transportServer.BindAddress = value;
			}
		}
		public int Port {
			get {
				return this._transportServer.BindPort;
			}
			set {
				this._transportServer.BindPort = value;
			}
		}
		public int Backlog {
			get {
				return this._transportServer.Backlog;
			}
			set {
				this._transportServer.Backlog = value;
			}
		}
		public ExceptionCallbackDelegate OnException {
			get {
				return _transportServer.OnException;
			}
			set {
				_transportServer.OnException = value;
			}
		}
		public AcceptCallbackDelegate OnAccept {
			get {
				return _transportServer.OnAccept;
			}
			set {
				_transportServer.OnAccept = value;
			}
		}
		public int MaxAlive {
			get {
				return _transportServer.MaxSocket;
			}
		}
		public int CurrentAlive {
			get {
				return _transportServer.SocketCount;
			}
		}
		public X509Certificate2 ServerCertificate {
			get {
				if (this.Secured) return ((TlsServer)this._transportServer)._Certificate;
				else return null;
			}
		}
		public bool Secured { get; private set; }
		public bool Running { get; private set; }

		public string ServerName = "";
		public Dictionary<string, string> Authusers = new Dictionary<string, string>();
		public RouterNode Router = new RouterNode();

		public OnRequestCallback OnRequest;
		public OnResponseCallback OnResponse;

		private TcpServer _transportServer;

		public HttpServer(long bind_addr, int port = 80, int backlog = 1000, int maxalive = 1000, X509Certificate2 cert = null) : this(new IPAddress(bind_addr), port, backlog, maxalive, cert) { }
		public HttpServer(byte[] bind_addr, int port = 80, int backlog = 1000, int maxalive = 1000, X509Certificate2 cert = null) : this(new IPAddress(bind_addr), port, backlog, maxalive, cert) { }
		public HttpServer(string bind_addr, int port = 80, int backlog = 1000, int maxalive = 1000, X509Certificate2 cert = null) : this(IPAddress.Parse(bind_addr), port, backlog, maxalive, cert) { }
		public HttpServer(IPAddress bind_addr, int port = 80, int backlog = 1000, int maxalive = 1000, X509Certificate2 cert = null) : this(new IPEndPoint(bind_addr, port), backlog, maxalive, cert) { }
		public HttpServer(int port = 80, X509Certificate2 cert = null) : this(new IPEndPoint(IPAddress.Any, port), 1000,  1000, cert) { }
		public HttpServer(X509Certificate2 cert) {
			this._transportServer = new TlsServer(443, cert);
			this.OnAccept += new AcceptCallbackDelegate(OnConnected);
			this.Secured = true;
		}
		public HttpServer(IPEndPoint bind_ep, int backlog = 1000, int maxalive = 1000, X509Certificate2 cert = null) {
			if(cert == null) {
				this._transportServer = new TcpServer(bind_ep, backlog, maxalive);
			}
			else {
				this._transportServer = new TlsServer(bind_ep, backlog, maxalive, cert);
				this.Secured = true;
			}
			this.OnAccept += new AcceptCallbackDelegate(OnConnected);
		}
		public HttpServer(HttpSetupInformation info) : this(info.BindEndPoint, info.Backlog, info.MaxAlive, (info.Certificate == null ? (info.CertificatePath == null ? null : new X509Certificate2(info.CertificatePath, info.CertificatePassword)) : info.Certificate)) {
			this.Router = info.HttpRoutingRoot;
			this.Authusers = info.AuthenticationDictionary;
			this.ServerName = info.ServerName;
		}

		public void ChangeServerSettings(HttpSetupInformation info) {
			lock(this.Router) {
				this.Router = info.HttpRoutingRoot;
			}
			this.Authusers = info.AuthenticationDictionary;
		}

		public void Start() {
			if(!Running) {
				Running = true;
				_transportServer.Start();
			}
		}

		public void Stop() {
			if (Running) {
				Running = false;
				_transportServer.Stop();
			}
		}
		
		private void OnConnected(SocketWrapper rs) {
			while (rs.Connected) {
				try {
					HttpRequest req = new HttpRequest(rs);
					if(rs.Connected && !req.ClientClosed) {
						if (!(OnRequest is null)) OnRequest.Invoke(req);
						HttpResponse res = new HttpResponse(req);
						res.SetHeader("Server", "Haron");
						if (req.KeepAlive)
							res.SetHeader("Connection", "keep-alive");
						else
							res.SetHeader("Connection", "close");
						try {
							bool found = Router.Route(req.URI, req, res);
							if (!found) res.NotFound();
							OnResponse?.Invoke(req, res);
						}
						catch (InvalidRequestException) {
							res.BadRequest();
						}
						catch (Exception e) {
							res.InternalServerError(e);
							OnException?.Invoke(e);
							//TODO: 500 에러 써드파티 핸들링 기능 추가
						}
						if (!res.isResponseEnd())
							res.End();
					}
					else if(req.ClientClosed) {
						rs.Close();
					}
				}
				catch(InvalidRequestException) {
					HttpResponse res = new HttpResponse(rs);
					res.BadRequest();
				}
				catch (SocketException e) {
					OnException(e);
				} catch (Exception e) {
					OnException(e);
				}
			}
		}
	}
}
