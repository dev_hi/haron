﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Server.Http;
using Haron.Server.Http.Packet;

namespace Haron.Server.Http.MiddleWare {
	public interface IMiddleWare {
		string GetDescription();
		bool OnResponse(HttpRequest req, HttpResponse res);
		bool OnException(Exception e);
		void OnCreate(string uri);
	}
}
