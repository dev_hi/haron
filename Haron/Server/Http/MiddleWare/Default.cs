﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Server.Http.Packet;
using System.IO;

namespace Haron.Server.Http.MiddleWare {
	public class Default : IMiddleWare {
		string dpath = ".\\www";

		public Default() {
		}
		public Default(string path) {
			this.dpath = path;
		}

		public string GetDescription() {
			return "기본 페이지입니다.";
		}

		public void OnCreate(string uri) {

		}

		public bool OnException(Exception e) {
			return false;
		}

		public bool OnResponse(HttpRequest req, HttpResponse res) {
			string path = dpath + req.URI.Replace('/', '\\');
			string[] indexfile = new string[] { 
				"index.htm",
				"index.html"
			};
			try {
				//dom.find("title")[0].SetText("Haron");
				//dom.find("#haron_hostname")[0].SetText(Environment.MachineName[0] + Environment.MachineName.ToLower().Substring(1));
				if ((File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory) {
					string b = null;
					for(int i = 0; i < indexfile.Length; ++i) {
						string ifp = Path.Combine(path, indexfile[i]);
						if (File.Exists(ifp)) {
							b = File.ReadAllText(ifp, Encoding.UTF8);
						}
					}

					if(!(b is null)) {
						res.SetHeader("Content-Type", "text/html;charset=utf-8");
						res.Send(Encoding.UTF8, b);
					}
					else {
						res.Forbidden();
					}
				} else {
					res.SendFile(path);
				}
			} catch (DirectoryNotFoundException) {
				res.StatusCode = HttpResponseCode.NotFound;
				res.Send(Encoding.Default, path + " 404 Not Found");
			} catch (FileNotFoundException) {
				res.StatusCode = HttpResponseCode.NotFound;
				res.Send(Encoding.Default, path + " 404 Not Found");
			}
			return false;
		}
	}
}
