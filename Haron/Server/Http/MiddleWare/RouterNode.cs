﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Haron.Server.Http.Packet;

namespace Haron.Server.Http.MiddleWare {
	public class RouterNode {
		public bool IsRoot {
			get {
				return GetParent() == null;
			}
		}
		private RouterNode Parent = null;
		private List<RouterNode> Children = new List<RouterNode>();
		private string CurrentUri;
		public List<IMiddleWare> RegisteredMiddleWare = new List<IMiddleWare>();
		
		public RouterNode() {
			this.CurrentUri = "/";
		}

		public RouterNode(RouterNode parent, string current_uri) {
			this.AppendTo(parent);
			this.CurrentUri = current_uri;
		}
		public RouterNode GetParent() {
			return this.Parent;
		}
		public RouterNode GetNode(string uri) {
			RouterNode ret = this;
			string[] tmp = uri.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
			if(tmp.Length == 0) 
				ret = this;
			else 
				foreach (RouterNode child in Children) {
					if (tmp[0] == child.CurrentUri) {
						string suburi = "";
						for (int i = 1; i < tmp.Length; ++i)
							suburi += "/" + tmp[i];
						ret = child.GetNode(suburi);
					}
					else {

					}
				}
			return ret;
		}
		public bool Route(string uri, HttpRequest req, HttpResponse res) {
			RouterNode rn = GetNode(uri);
			if(rn == null) {
				return false;
			}
			else if (rn.RegisteredMiddleWare.Count == 0) {
				return false;
			}
			else {
				req.InURI = req.URI.Replace(rn.GetAllUriPath(), "");
				foreach (IMiddleWare mw in rn.RegisteredMiddleWare) {
					if (!mw.OnResponse(req, res))
						break;
				}
			}
			return true;
		}
		public string GetAllUriPath() {
			return (this.IsRoot ? "" : GetParent().GetAllUriPath() + (GetParent().IsRoot ? "" : "/")) + this.CurrentUri;
		}
		public void RegisterMiddleWare(string uri, IMiddleWare mw) {
			RouterNode target = (uri == "./" ? this : this.CreateNode(uri));
			if (target.GetMiddleWare(mw.GetType()) == null) {
				target.RegisteredMiddleWare.Add(mw);
			}
		}
		public void RegisterMiddleWare(string uri, IMiddleWare mw, int i) {
			RouterNode target = (uri == "./" ? this : this.CreateNode(uri));
			if (target.GetMiddleWare(mw.GetType()) == null) {
				target.RegisteredMiddleWare.Insert(i, mw);
			}
		}
		public IMiddleWare GetMiddleWare(Type type) {
			IMiddleWare found = null;
			for (int i = 0; i < this.RegisteredMiddleWare.Count; ++i) {
				found = (this.RegisteredMiddleWare[i].GetType().Name == type.Name ? this.RegisteredMiddleWare[i] : null);
				if (found != null) break;
			}
			return found;
		}
		public bool IsExistNode(string uri) {
			bool ret = false;
			string[] tmp = uri.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
			if (tmp.Length == 0)
				ret = true;
			else
				foreach (RouterNode child in Children) {
					if (tmp[0] == child.CurrentUri) {
						string suburi = "";
						for (int i = 1; i < tmp.Length; ++i)
							suburi += "/" + tmp[i];
						ret = child.IsExistNode(suburi);
					} else {

					}
				}
			return ret;
		}
		public RouterNode CreateNode(string uri) {
			RouterNode ret;
			if (!IsExistNode(uri)) {
				string[] tmp = uri.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
				string suburi = "";
				for (int i = 1; i < tmp.Length; ++i)
					suburi += "/" + tmp[i];
				ret = (IsExistNode(tmp[0]) ? GetNode(tmp[0]) : new RouterNode(this, tmp[0])).CreateNode(suburi);
			}
			else ret = GetNode(uri);
			
			return ret;
		}
		public void Append(RouterNode node) {
			node.AppendTo(this);
		}
		public void AppendTo(RouterNode parent) {
			this.Parent = parent;
			this.Parent.Children.Add(this);
		}
		public void AppendTo(RouterNode node, string uri) {
			node.GetNode(uri).Append(this);
		}
		public void Remove() {
			this.Parent.Children.Remove(this);
			this.Parent = null;
			this.CurrentUri = "";
		}
		public void Remove(string uri) {
			this.GetNode(uri).Remove();
		}

		public int ChildCount {
			get {
				return this.Children.Count;
			}
		}
		public RouterNode GetChild(int i) {
			return this.Children[i];
		}
		public override string ToString() {
			string m = "";
			if(this.RegisteredMiddleWare.Count == 0) {
				m = "없음";
			}
			else {
				for (int i = 0; i < this.RegisteredMiddleWare.Count; ++i) {
					m += this.RegisteredMiddleWare[i].GetType().Name;
					if (i != this.RegisteredMiddleWare.Count - 1) m += ", ";
				}
			}
			return "라우팅노드 (URI : " + this.GetAllUriPath() + "), 미들웨어 : " + m;
		}
	}
}
