﻿using Haron.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Server.Http.MiddleWare {
	public static class MiddlewareManager {
		private static List<Type> _CreatableWare = new List<Type>(Assembly.GetExecutingAssembly().GetTypes().Where<Type>(new Func<Type, bool>(_IMiddlewareFilter)));

		private static bool _IMiddlewareFilter(Type t) {
			return t.GetInterface(typeof(IMiddleWare).FullName) != null;
		}

		public static Type[] GetAllMiddlewareTypes() {
			Type[] types = PluginManager.GetImportedClasses(typeof(IMiddleWare));
			return types.Concat(_CreatableWare).ToArray();
		}
	}
}
