﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Server.Http.Packet;
using Haron.Authorization;

namespace Haron.Server.Http.MiddleWare {
	public class HttpAuth : IMiddleWare {
		private IAuthentication AuthenticationMethod;

		public HttpAuth() {
		}

		public HttpAuth(IAuthentication auth_method) {
			this.AuthenticationMethod = auth_method;
		}

		public IAuthentication GetAuthenticationMethod() {
			return this.AuthenticationMethod;
		}

		public string GetDescription() => "";

		public void OnCreate(string uri) {
			//throw new NotImplementedException();
		}

		public bool OnException(Exception e) {
			//throw new NotImplementedException();
			return true;
		}

		public bool OnResponse(HttpRequest req, HttpResponse res) {
			bool authorized = req.HeaderExist("Authorization");

			if(authorized) {
				try {
					string plain = Encoding.Default.GetString(Convert.FromBase64String(req.GetHeader("Authorization").Substring(6)));
					string[] tmp = plain.Split(new char[] { ':' });
					authorized = this.AuthenticationMethod.Authenticate(tmp[0], tmp[1]);
				}
				catch (Exception e) {
					authorized = false;
					res.InternalServerError();
				}
			}

			if (!authorized) {
				res.StatusCode = HttpResponseCode.Unauthorized;
				res.SetHeader("WWW-Authenticate", "Basic realm=\"fuck off\"");
				res.Send(Encoding.Default, HttpUtils.GetHttpCodeString(HttpResponseCode.Unauthorized));
			}
			return authorized;
		}
	}
}
