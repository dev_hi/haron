﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Haron.Server.Packet;

namespace Haron.Server.Builder {
	public class TotalUsage : IMiddleWare {
		public bool OnResponse(HttpRequest req, HttpResponse res) {
			if(req.Method == HttpMethod.POST) {
				res.Send(Encoding.Default, req.GetBodyString());
			}
			else {
				res.NotFound();
			}
			return true;
		}
	}
}
