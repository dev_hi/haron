﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Server.Http.Packet;
using Haron.Parser;
using System.Text.RegularExpressions;

namespace Haron.Server.Http.MiddleWare {
	public delegate JSON RestAPIEndpointRequestedCallback(HttpRequest req, HttpResponse res, string[] args);
	public class RestAPI : IMiddleWare {
		private Dictionary<string, RestAPIEndpointRequestedCallback>[] endpoints = new Dictionary<string, RestAPIEndpointRequestedCallback>[9];

		public RestAPI() {
			endpoints[(int)HttpMethod.GET] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.POST] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.PUT] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.DELETE] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.HEAD] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.OPTIONS] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.CONNECT] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.TRACE] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
			endpoints[(int)HttpMethod.PATCH] = new Dictionary<string, RestAPIEndpointRequestedCallback>();
		}

		public void SetEndpoint(HttpMethod method, string endpoint, RestAPIEndpointRequestedCallback onResponse) {
			if (!endpoints[(int)method].ContainsKey(endpoint)) {
				endpoints[(int)method].Add(endpoint, onResponse);
			}
		}
		public bool OnResponse(HttpRequest req, HttpResponse res) {
			string endpoint = req.URI.Replace("/api", "");
			JSON s;
			if (endpoint.Length == 0) {
				res.StatusCode = HttpResponseCode.NotFound;
				s = new JSON();
				s["message"] = new JSON("api endpoint not found");
				res.SetHeader("Content-Type", "application/json");
				res.Send(Encoding.Default, s.ToString());
				return false;
			}
			string k = null;
			RestAPIEndpointRequestedCallback v = null;
			foreach (KeyValuePair<string, RestAPIEndpointRequestedCallback> kv in endpoints[(int)req.Method])  {
				int sidx = endpoint.IndexOf('/', 1);
				sidx = sidx == -1 ? endpoint.Length : sidx;
				if (kv.Key == endpoint.Substring(0, sidx)) {
					k = kv.Key;
					v = kv.Value;
				}
			}
			if (!(k is null)) {
				s = v(req, res, endpoint.Replace(k, "").Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));
			}
			else {
				res.StatusCode = HttpResponseCode.NotFound;
				s = new JSON();
				s["message"] = new JSON("api endpoint not found");
			}
			if(!(s is null)) {
				res.SetHeader("Content-Type", "application/json");
				res.Send(Encoding.Default, s.ToString());
			}
			return false;
		}

		public string GetDescription() {
			return "Rest API용 엔진입니다.";
		}

		public bool OnException(Exception e) {
			return false;
		}

		public void OnCreate(string uri) {

		}
	}
}
