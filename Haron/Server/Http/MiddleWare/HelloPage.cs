﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haron.Server.Http.Packet;
using System.IO;
using Haron.Parser;

namespace Haron.Server.Http.MiddleWare {
	public class HelloPage : IMiddleWare {
		public string GetDescription() {
			return "첫 설치 페이지입니다.";
		}

		public void OnCreate(string uri) {

		}

		public bool OnException(Exception e) {
			return false;
		}

		public bool OnResponse(HttpRequest req, HttpResponse res) {
			string html = Properties.Resources.hellopage2;
			HtmlDomObject dom = new HtmlDomObject(html);
			HtmlDomNode[] found = dom.find("data#Win32_Processor");
			if(found.Length > 0) {
				found[0].html("<p>TEST STRING</p>");
				found[1].html("<p>TEST STRING</p>");
			}
			res.SetHeader("Content-Type", "text/html;charset=utf-8");
			res.Send(Encoding.UTF8, dom.ToString());
			return false;
		}
	}
}
