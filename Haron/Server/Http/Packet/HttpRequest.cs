﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Sockets;
using System.Net;
using Haron.Server;
using System.Net.Security;

namespace Haron.Server.Http.Packet {
	public class HttpRequest {
		public IPAddress RemoteAddress {
			get {
				return ((IPEndPoint)this.RemoteSocket.RemoteEndPoint).Address;
			}
		}
		public int RemotePort {
			get {
				return ((IPEndPoint)this.RemoteSocket.RemoteEndPoint).Port;
			}
		}
		public bool KeepAlive {
			get {
				bool ret = false;
				if(HeaderFields.ContainsKey("Connection")) 
					ret = HeaderFields["Connection"] == "keep-alive";
				return ret;
			}
			set { }
		}
		public HttpMethod Method;
		public string URI;
		public string InURI;
		public string HttpVersionString;
		private Dictionary<string, string> HeaderFields = new Dictionary<string, string>();
		private string HeaderString;
		private byte[] HeaderBuffer;
		private byte[] BodyBuffer;
		private SocketWrapper RemoteSocket;
		public bool BodyExist = false;
		public bool Secured = false;

		public Dictionary<string, string> UriParam = new Dictionary<string, string>();
		public string ContentType = null;
		public Dictionary<string, string> BodyParam = new Dictionary<string, string>();
		
		public List<object> AdditionalObjects = new List<object>();

		public bool ClientClosed = false;

		/**
		 * <summary>리퀘스트</summary>
		 * <param name="header">헤더 버퍼</param>
		 * <param name="rs">리모트 소켓</param>
		 */

		 public HttpRequest(SocketWrapper rs) {
			this.Secured = rs.Secured;
			if (rs.Connected && !ClientClosed) {
				MemoryStream ms = new MemoryStream();
				bool headerend = false;
				int headerend_idx = 0;
				do {
					byte[] buffer = new byte[rs.ReceiveBufferSize];
					int recv = rs.Receive(buffer, buffer.Length, SocketFlags.None);
					ClientClosed = recv == 0;
					if (!ClientClosed) {
						for (int i = 0; i < recv - 3 && !headerend; ++i) {
							if (headerend = ((buffer[i] == 0x0D && buffer[i + 1] == 0x0A && buffer[i + 2] == 0x0D && buffer[i + 3] == 0x0A) || headerend))
								headerend_idx = i + 4;
						}
						ms.Write(buffer, 0, recv);
					}
				} while (!headerend && rs.Connected && !ClientClosed);


				string uri = "";
				this.RemoteSocket = rs;
				HeaderBuffer = new byte[headerend_idx];
				ms.Position = 0;
				ms.Read(HeaderBuffer, 0, headerend_idx);
				HeaderString = Encoding.ASCII.GetString(HeaderBuffer);

				string[] hs = HeaderString.Split(new string[] { "\r\n" }, StringSplitOptions.None);
				for (int i = 0; i < hs.Length; ++i) {
					if (i == 0) {
						string[] tmp = hs[i].Split(new char[] { ' ' });
						if (HttpMethod.GET.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.GET;
						if (HttpMethod.POST.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.POST;
						if (HttpMethod.HEAD.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.HEAD;
						if (HttpMethod.PUT.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.PUT;
						if (HttpMethod.CONNECT.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.CONNECT;
						if (HttpMethod.DELETE.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.DELETE;
						if (HttpMethod.OPTIONS.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.OPTIONS;
						if (HttpMethod.PATCH.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.PATCH;
						if (HttpMethod.TRACE.ToString() == tmp[0].ToUpper())
							Method = HttpMethod.TRACE;
						uri = tmp[1];
						HttpVersionString = tmp[2];
					}
					else if (hs[i].Length == 0) {
						break;
					}
					else {
						string[] tmp = hs[i].Split(new string[] { ":" }, StringSplitOptions.None);
						string key = tmp[0].Trim();
						string val = tmp[1].Trim();
						if (this.HeaderFields.ContainsKey(key))
							this.HeaderFields[key] = val;
						else
							this.HeaderFields.Add(key, val);
					}
				}

				int j = uri.IndexOf('?');
				j = j == -1 ? uri.Length : j + 1;
				URI = uri.Substring(0, j == uri.Length ? j : j - 1);
				UriParam = ParseUrlencoded(uri.Substring(j, uri.Length - j));
				
				if (this.HeaderFields.ContainsKey("Content-Length")) {
					this.BodyExist = true;
					int bodylen = 0;
					bool parse_succ = int.TryParse(GetHeader("Content-Length"), out bodylen);
					if (parse_succ) {
						BodyBuffer = new byte[bodylen];
						int r = ms.Read(BodyBuffer, 0, (int)(ms.Length - headerend_idx));
						ms.Close();
						ms = new MemoryStream(BodyBuffer);
						while (r < BodyBuffer.Length) {
							byte[] buf = new byte[rs.ReceiveBufferSize];
							int recv = rs.Receive(buf, bodylen > rs.ReceiveBufferSize ? rs.ReceiveBufferSize : bodylen, SocketFlags.None);
							ms.Write(buf, 0, recv);
							r += recv;
						}
						BodyBuffer = ms.ToArray();
						if (this.HeaderFields.ContainsKey("Content-Type")) {
							this.ContentType = this.HeaderFields["Content-Type"];
							if (this.HeaderFields["Content-Type"].IndexOf("application/x-www-form-urlencoded") > -1) {
								BodyParam = ParseUrlencoded(GetBodyString());
							}
							else if (this.HeaderFields["Content-Type"].IndexOf("multipart/form-data") == 0) {
								
							}
						}
					}
					else {
						throw new InvalidRequestException(this);
					}
				}
				ms.Close();
			}
		}
		
		public Dictionary<string, string> ParseUrlencoded(string urlencoded) {
			Dictionary<string, string> ret = new Dictionary<string, string>();
			string[] tmp = urlencoded.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
			for(int i = 0; i < tmp.Length && tmp.Length > 0; ++i) {
				int j = tmp[i].IndexOf('=');
				j = j >= 0 ? j + 1 : tmp[i].Length ;
				ret.Add(tmp[i].Substring(0, j == 0 ? j : j - 1), tmp[i].Substring(j, tmp[i].Length - j));
			}
			return ret;
		}

		public string GetHeader(string key) {
			return this.HeaderFields[key];
		}

		public bool HeaderExist(string key) {
			return this.HeaderFields.ContainsKey(key);
		}

		public void BadRequest() {
			HttpResponse res = new HttpResponse(this);
			res.BadRequest();
		}

		public byte[] GetBody() {
			return this.BodyBuffer;
		}

		public string GetBodyString() {
			Encoding enc = Encoding.Default;
			string hdr = GetHeader("Content-Type");
			int i = hdr.IndexOf("charset=");
			if (i > -1) {
				i += 8;
				string encstr = hdr.Substring(i).ToLower();
				if(encstr.IndexOf("utf-8") == 0) {
					enc = Encoding.UTF8;
				}
			}
			return enc.GetString(this.BodyBuffer);
		}

		public SocketWrapper GetRemoteSocket() {
			return this.RemoteSocket;
		}
	}
}
