﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Sockets;

namespace Haron.Server.Http.Packet {
	public class WebSocketPacket {

		public bool FIN;
		public bool RSV1;
		public bool RSV2;
		public bool RSV3;
		public bool OPCODE;
		public bool MASKED;
		public uint LEN;
		public ulong LEN_EXT;
		public ulong LENGTH;
		public byte[] MASK_KEY;

		public byte[] PAYLOAD;
		public byte[] DATA;

		public WebSocketPacket PACKET_CONTINUE;
		/*
		      0                   1                   2                   3
			  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
			 +-+-+-+-+-------+-+-------------+-------------------------------+
			 |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
			 |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
			 |N|V|V|V|       |S|             |   (if payload len==126/127)   |
			 | |1|2|3|       |K|             |                               |
			 +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
			 |     Extended payload length continued, if payload len == 127  |
			 + - - - - - - - - - - - - - - - +-------------------------------+
			 |                               |Masking-key, if MASK set to 1  |
			 +-------------------------------+-------------------------------+
			 | Masking-key (continued)       |          Payload Data         |
			 +-------------------------------- - - - - - - - - - - - - - - - +
			 :                     Payload Data continued ...                :
			 + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
			 |                     Payload Data continued ...                |
			 +---------------------------------------------------------------+

			  * OPCODES *
			 
			  *  %x0 denotes a continuation frame

			  *  %x1 denotes a text frame

			  *  %x2 denotes a binary frame

			  *  %x8 denotes a connection close

			  *  %x9 denotes a ping

			  *  %xA denotes a pong
		 */

		public WebSocketPacket(System.Net.Sockets.Socket rs) {
			byte[] buffer = new byte[2];
			rs.Receive(buffer, 2, SocketFlags.None);
			//FIN = buffer[0]

			if(MASKED) {
				DATA = new byte[PAYLOAD.Length];
				for(int i = 0; i < PAYLOAD.Length; ++i) {
					DATA[i] = (byte)(PAYLOAD[i] ^ MASK_KEY[i % 4]);
				}
			}
			else {
				DATA = PAYLOAD;
			}
		}

		public static bool GetBit(byte b, int bit) {
			return (b & (1 << bit)) != 0;
		}
		public static byte GetBits(byte b, int s, int e) {
			/* 10110101
			 * 11100000 AND
			 * 10000000
			 * 00000010 << 1
			 * s = 5, e = 7
			 * for(int i = s; i < e; ++i) { a &=  }
			 */
			int a = e;
			for (int i = s; i < e; ++i)
				a |= (1 << i);
			return (byte)(b & a);
		}
	}
}
