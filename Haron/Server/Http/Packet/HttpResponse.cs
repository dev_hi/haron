﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.Mime;
using System.IO;
using System.Net.Security;

namespace Haron.Server.Http.Packet {
	public class HttpResponse {
		/**
		 * <summary>HTTP 응답코드를 가져오거나 설정합니다.</summary>
		 * <exception cref="HeaderAlreadySentException"></exception>
		 */
		public HttpResponseCode StatusCode {
			get {
				return statuscode;
			}
			set {
				if (HeaderSent) {
					throw new HeaderAlreadySentException();
				} else {
					statuscode = value;
				}
			}
		}
		/**
		 * <summary>버퍼 내용을 한번에 보낼수 없을 경우 청크로 나눠서 보냅니다.</summary>
		 * <exception cref="HeaderAlreadySentException"></exception>
		 */
		public bool ChunkedTransfer { 
			get {
				return GetHeader("Transfer-Encoding") == "chunked";
			}
			set {
				if (value)
					SetHeader("Transfer-Encoding", "chunked");
				else
					RemoveHeader("Transfer-Encoding");
			}
		}


		private HttpResponseCode statuscode = HttpResponseCode.OK;
		private HttpRequest Request;
		private Dictionary<string, string> HeaderFields = new Dictionary<string, string>();
		private MemoryStream OutputBufferStream = new MemoryStream();
		private SocketWrapper RemoteSocket;
		private bool ResponseEnd = false;

		private bool HeaderSent = false;

		public bool Secured = false;
		private SslStream SecuredStream;
		public HttpResponse(HttpRequest req) {
			this.Request = req;
			this.Secured = this.Request.Secured;
			this.RemoteSocket = this.Request.GetRemoteSocket();
		}
		public HttpResponse(SocketWrapper sock) {
			this.RemoteSocket = sock;
			this.Secured = this.Request.Secured;
		}

		public bool HeaderExists(string key) {
			return HeaderFields.ContainsKey(key);
		}

		public void SetHeader(string key, string value) {
			if (HeaderSent)
				throw new HeaderAlreadySentException();
			else {
				if (HeaderFields.ContainsKey(key)) {
					HeaderFields[key] = value;
				} else {
					HeaderFields.Add(key, value);
				}
			}
		}

		public string GetHeader(string key) {
			if (HeaderFields.ContainsKey(key))
				return HeaderFields[key];
			return null;
		}

		public void RemoveHeader(string key) {
			if (HeaderFields.ContainsKey(key))
				HeaderFields.Remove(key);
		}

		public void SendHeader() {
			if(HeaderSent) 
				throw new HeaderAlreadySentException();
			else if(RemoteSocket.Connected) {
				string hdr = "";
				hdr += "HTTP/1.1 " + ((int)statuscode) + " " + HttpUtils.GetHttpCodeString(statuscode) + "\r\n";
				foreach(KeyValuePair<string, string> kv in HeaderFields) {
					hdr += kv.Key + ": " + kv.Value + "\r\n";
				}
				hdr += "\r\n";
				byte[] buffer = Encoding.Default.GetBytes(hdr);
				RemoteSocket.Send(buffer);
				HeaderSent = true;
			}
			else {
				Thread.CurrentThread.Abort();
			}
		}

		public void Send(Encoding enc, string str) {
			Send(enc.GetBytes(str));
		}

		public void SendFile(string path) {
			try {
				int chunksize = RemoteSocket.SendBufferSize;
				FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, chunksize);
				this.SetHeader("Content-Type", HttpUtils.GetMIMEType(Path.GetFileName(path)));
				if (fs.Length > chunksize) ChunkedTransfer = true;
				for(int t = 0; t < fs.Length;) {
					byte[] buffer = new byte[chunksize];
					int read = fs.Read(buffer, 0, chunksize);
					t += Send(buffer, 0, read);
				}
			}
			catch (FileNotFoundException) {
				NotFound();
			}
			catch (DirectoryNotFoundException) {
				NotFound();
			}
		}

		public void Send(byte[] buffer) {
			Send(buffer, 0, buffer.Length);
		}

		public int Send(byte[] buffer, int offset, int size) {
			int ret = 0;
			if (RemoteSocket.Connected) {
				if(size > RemoteSocket.SendBufferSize && !ChunkedTransfer) {
					ChunkedTransfer = true;
					int sent = 0;
					for(int i = 0; i < size / RemoteSocket.SendBufferSize; ++i) {
						sent += Send(buffer, sent, RemoteSocket.SendBufferSize);
					}
					Send(buffer, sent, size - sent);
				}
				else if (ChunkedTransfer) {
					if (!HeaderSent) {
						SendHeader();
					}
					RemoteSocket.Send(Encoding.Default.GetBytes(String.Format("{0:X}\r\n", size)));
					ret = RemoteSocket.Send(buffer, offset, size, SocketFlags.None);
					RemoteSocket.Send(new byte[] { 0x0D, 0x0A });
				} else if (HeaderFields.ContainsKey("Content-Length")) {
					if (!HeaderSent) {
						SendHeader();
					}
					ret = RemoteSocket.Send(buffer, int.Parse(GetHeader("Content-Length")), SocketFlags.None);
				} else {
					WriteBuffer(buffer, offset, size);
					ret = size;
				}
			} else {
				Thread.CurrentThread.Abort();
			}
			return ret;
		}

		public void WriteBuffer(byte[] buffer) {
			WriteBuffer(buffer, 0, buffer.Length);
		}

		public void WriteBuffer(byte[] buffer, int offset, int size) {
			OutputBufferStream.Write(buffer, offset, size);
		}

		public void FlushBuffer() {
			if(!ChunkedTransfer)
				this.SetHeader("Content-Length", OutputBufferStream.Length.ToString());
			byte[] buffer = OutputBufferStream.GetBuffer();
			Send(buffer, 0, (int)OutputBufferStream.Length);
			NewBuffer();
		}

		public void NewBuffer() {
			OutputBufferStream.Close();
			OutputBufferStream = new MemoryStream();
		}

		public void End() {
			if (!ResponseEnd) {
				FlushBuffer();
				if(this.Request == null) 
					RemoteSocket.Close();
				else if (!this.Request.KeepAlive)
					RemoteSocket.Close();
				
				ResponseEnd = true;
			}
		}

		public bool isResponseEnd() {
			return ResponseEnd;
		}

		public string GetBufferString(Encoding enc) {
			return enc.GetString(OutputBufferStream.GetBuffer());
		}

		public void NotFound() {
			statuscode = HttpResponseCode.NotFound;
			Send(Encoding.Default, "404 Not Found");
		}
		public void Forbidden() {
			statuscode = HttpResponseCode.Forbidden;
			Send(Encoding.Default, "403 Forbidden");
		}

		public void InternalServerError() {
			statuscode = HttpResponseCode.InternalServerError;
			Send(Encoding.Default, "500 Internal Server Error");
		}

		public void InternalServerError(Exception e) {
			statuscode = HttpResponseCode.InternalServerError;
			Send(Encoding.Default, "500 Internal Server Error\n" + e.Message);
		}

		public void BadRequest() {
			statuscode = HttpResponseCode.BadRequest;
			Send(Encoding.Default, "400 Bad Request");
		}
	}
}
