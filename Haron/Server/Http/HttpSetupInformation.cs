﻿using Haron.Authorization;
using Haron.Server.Http.MiddleWare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Server.Http {
	public struct HttpSetupInformation {
		public string ServerName;
		public int Backlog;
		public IPEndPoint BindEndPoint;
		public RouterNode HttpRoutingRoot;
		public Dictionary<string, string> AuthenticationDictionary;
		public int MaxAlive;
		public string CertificatePath;
		public SecureString CertificatePassword;
		public X509Certificate2 Certificate;
	}
}
