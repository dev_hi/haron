﻿using Haron.Server.Http.Packet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Server.Http {
	public class InvalidRequestException : Exception {
		public HttpRequest Request;

		public InvalidRequestException() {

		}

		public InvalidRequestException(HttpRequest req) {
			this.Request = req;
		}
	}
}
