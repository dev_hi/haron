﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Server {
	public class SocketWrapper {
		public Socket _InnerSocket;
		public SslStream _secureSocket;
		public bool Secured = false;

		public SocketWrapper(Socket socket) {
			this._InnerSocket = socket;
		}
		public SocketWrapper(SslStream secured, Socket socket) : this(socket) {
			this._secureSocket = secured;
			this.Secured = true;
		}

		public int ReceiveBufferSize => this._InnerSocket.ReceiveBufferSize;
		public bool Connected => this._InnerSocket.Connected;
		public EndPoint RemoteEndPoint => this._InnerSocket.RemoteEndPoint;
		public int SendBufferSize => this._InnerSocket.SendBufferSize;

		public int Receive(byte[] buffer) {
			if (Secured) {
				return this._secureSocket.Read(buffer, 0, buffer.Length);
			}
			else {
				return this._InnerSocket.Receive(buffer);
			}
		}

		public int Receive(byte[] buffer, int length, SocketFlags flag) {
			if (Secured) {
				return this._secureSocket.Read(buffer, 0, length);
			}
			else {
				return this._InnerSocket.Receive(buffer, length, flag);
			}
		}

		public void Close() {
			if (Secured) { this._secureSocket.Close(); }
			else { this._InnerSocket.Close(); }
		}

		public static implicit operator SocketWrapper(Socket v) {
			return new SocketWrapper(v);
		}

		public int Send(byte[] buffer) {
			if (Secured) {
				this._secureSocket.Write(buffer);
				return buffer.Length;
			}
			else {
				return this._InnerSocket.Send(buffer);
			}
		}

		public int Send(byte[] buffer, int size, SocketFlags flag) {
			if (Secured) {
				this._secureSocket.Write(buffer, 0, size);
				return buffer.Length;
			}
			else {
				return this._InnerSocket.Send(buffer, size, flag);
			}
		}

		public int Send(byte[] buffer, int offset, int size, SocketFlags flag) {
			if(Secured) {
				this._secureSocket.Write(buffer, offset, size);
				return buffer.Length;
			}
			else {
				return this._InnerSocket.Send(buffer, offset, size, flag);
			}
		}

		public void Disconnect(bool v) {
			if (Secured) {
				this._secureSocket.Close();
			}
			else {
				this._InnerSocket.Disconnect(v);
			}
		}
	}
}
