﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace Haron.Server {
	public delegate void AcceptCallbackDelegate(SocketWrapper remote_socket);
	public delegate void ExceptionCallbackDelegate(Exception e);

	public class TcpServer : IServer {
		public IPAddress BindAddress {
			get {
				return this.BindEndPoint.Address;
			}
			set {
				this.BindEndPoint.Address = value;
			}
		}

		public int BindPort {
			get {
				return this.BindEndPoint.Port;
			}
			set {
				this.BindEndPoint.Port = value;
			}
		}
		public int MaxSocket {
			get {
				return this.sockPool.MaxSocket;
			}
		}
		public int SocketCount {
			get {
				return this.sockPool.GetSocketCount();
			}
		}

		public int Backlog = 1000;
		public bool Running = false;
		public int Timeout = 2000;

		public AcceptCallbackDelegate OnAccept;
		public ExceptionCallbackDelegate OnException;

		protected IPEndPoint BindEndPoint;
		protected Socket ListenSocket;
		protected Thread ListenThread;
		protected SocketPool sockPool = new SocketPool(1000);

		public TcpServer(long bind_addr, int port, int backlog, int maxsock) : this(new IPAddress(bind_addr), port, backlog, maxsock) { }
		public TcpServer(byte[] bind_addr, int port, int backlog, int maxsock) : this(new IPAddress(bind_addr), port, backlog, maxsock) { }
		public TcpServer(string bind_addr, int port, int backlog, int maxsock) : this(IPAddress.Parse(bind_addr), port, backlog, maxsock) { }
		public TcpServer(IPAddress bind_addr, int port, int backlog, int maxsock) : this(new IPEndPoint(bind_addr, port), backlog, maxsock) { }
		public TcpServer(IPAddress bind_addr, int port, int backlog) : this(new IPEndPoint(bind_addr, port), backlog) { }
		public TcpServer(IPEndPoint bind_ep, int backlog, int maxsock) : this(bind_ep, backlog) { this.sockPool = new SocketPool(maxsock); }
		public TcpServer(IPEndPoint bind_ep, int backlog) : this(bind_ep) { this.Backlog = backlog; }
		public TcpServer(int port) : this(new IPEndPoint(IPAddress.Any, port)) { }
		public TcpServer(IPEndPoint bind_ep) {
			this.BindEndPoint = bind_ep;
			this.NewSocketInstance();
		}
		protected TcpServer() {

		}


		public void Start() {
			try {
				if (!Running) {
					if (this.ListenSocket == null) this.NewSocketInstance();
					this.ListenSocket.Bind(this.BindEndPoint);
					this.ListenSocket.Listen(Backlog);
					this.Running = true;
					if (this.ListenSocket.IsBound) {
						this.ListenThread.Start();
					}
				}
			}
			catch (Exception e) {
				this.OnException?.Invoke(e);
			}
		}

		private void ListenThreadWorker() {
			while (this.ListenSocket.IsBound && this.Running) {
				try {
					System.Net.Sockets.Socket accepted = this.sockPool.Enqueue(this.ListenSocket.Accept());
					ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadWaitCallback), accepted);
				}
				catch (ThreadInterruptedException e) { }
				catch (ThreadAbortException e) { }
				catch (Exception e) {
					if(OnException != null) OnException(e);
					Stop();
				}
			}
		}

		public void Stop() {
			try {
				if (Running) {
					this.Running = false;
					this.ListenSocket.Close();
					this.ListenThread.Interrupt();
					this.ListenThread.Abort();
					this.sockPool.Destroy();
					this.NewSocketInstance();
				}
			}
			catch (Exception e) {
				this.OnException?.Invoke(e);
			}
		}
	

		protected void ThreadWaitCallback(object arg) {
			SocketWrapper sock = new SocketWrapper((Socket)arg);
			sock._InnerSocket.ReceiveTimeout = this.Timeout;
			sock._InnerSocket.SendTimeout = this.Timeout;
			try {
				this.OnAccept(sock);
			}
			catch(Exception e) {
				if (sock.Connected) sock.Disconnect(false);
				OnException?.Invoke(e);
			}
		}

		protected void NewSocketInstance() {
			if (this.Running)
				this.Stop();
			this.ListenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			this.ListenThread = new Thread(new ThreadStart(ListenThreadWorker));
		}
	}
}
