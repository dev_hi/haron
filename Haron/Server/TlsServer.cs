﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net.NetworkInformation;
using System.Threading;
using System.Security.Authentication;

namespace Haron.Server {
	public delegate void OnAuthenticatedCallback(SslStream stream);
	public class TlsServer : TcpServer {
		public X509Certificate2 _Certificate;
		public OnAuthenticatedCallback OnAuthenticated;

		public TlsServer(long bind_addr, int port, int backlog, int maxsock, X509Certificate2 cert) : this(new IPAddress(bind_addr), port, backlog, maxsock, cert) { }
		public TlsServer(byte[] bind_addr, int port, int backlog, int maxsock, X509Certificate2 cert) : this(new IPAddress(bind_addr), port, backlog, maxsock, cert) { }
		public TlsServer(string bind_addr, int port, int backlog, int maxsock, X509Certificate2 cert) : this(IPAddress.Parse(bind_addr), port, backlog, maxsock, cert) { }
		public TlsServer(IPAddress bind_addr, int port, int backlog, int maxsock, X509Certificate2 cert) : this(new IPEndPoint(bind_addr, port), backlog, maxsock, cert) { }
		public TlsServer(IPAddress bind_addr, int port, int backlog, X509Certificate2 cert) : this(new IPEndPoint(bind_addr, port), backlog, cert) { }
		public TlsServer(IPEndPoint bind_ep, int backlog, int maxsock, X509Certificate2 cert) : this(bind_ep, backlog, cert) { this.sockPool = new SocketPool(maxsock); }
		public TlsServer(IPEndPoint bind_ep, int backlog, X509Certificate2 cert) : this(bind_ep, cert) { this.Backlog = backlog; }
		public TlsServer(int port, X509Certificate2 cert) : this(new IPEndPoint(IPAddress.Any, port), cert) { }
		public TlsServer(IPEndPoint bind_ep, X509Certificate2 cert) {
			this.BindEndPoint = bind_ep;
			this._Certificate = cert;
			this.NewSocketInstance();
		}

		private void ListenThreadWorker() {
			while (this.ListenSocket.IsBound && this.Running) {
				try {
					System.Net.Sockets.Socket accepted = this.sockPool.Enqueue(this.ListenSocket.Accept());
					ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadWaitCallback), accepted);
				}
				catch (ThreadInterruptedException e) { }
				catch (ThreadAbortException e) { }
				catch (Exception e) {
					OnException?.Invoke(e);
					Stop();
				}
			}
		}

		protected new void NewSocketInstance() {
			if (this.Running)
				this.Stop();
			this.ListenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			this.ListenThread = new Thread(new ThreadStart(ListenThreadWorker));
		}

		protected new void ThreadWaitCallback(object arg) {
			Socket sock = (Socket)arg;
			try {
				SslStream stream = new SslStream(new NetworkStream(sock), false);
				try { stream.AuthenticateAsServer(this._Certificate, false, SslProtocols.Tls12, false); } catch (AuthenticationException) { }
				this.OnAccept(new SocketWrapper(stream, sock));
			}
			catch (Exception e) {
				try { if (sock.Connected) sock.Disconnect(false); } catch (Exception) { }
				OnException?.Invoke(e);
			}
		}
	}
}
