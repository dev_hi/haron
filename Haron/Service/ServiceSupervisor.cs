﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration.Install;
using System.ServiceProcess;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections;
using System.Reflection;
using Haron.Settings;

namespace Haron.Service {
	public static class ServiceSupervisor {
		public static string ServiceName {
			get {
				return AppSettings.Current.ServiceName;
			}
			set {
				AppSettings.Current.ServiceName = value;
				AppSettings.Current.Save();
			}
		}
		public static string DisplayName {
			get {
				
				return AppSettings.Current.DisplayName;
			}
			set {
				AppSettings.Current.DisplayName = value;
				AppSettings.Current.Save();
			}
		}
		public static string Description {
			get {
				return AppSettings.Current.Description;
			}
			set {
				AppSettings.Current.Description = value;
				AppSettings.Current.Save();
			}
		}
		public static bool IsInstalled { 
			get {
				return GetService() != null;
			}
			set { }
		}

		public static Exception LastInstallHelperException;

		public static bool InstallService() {
			try {
				if (!IsInstalled) {
					ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
					return true;
				}
				else return false;
			}
			catch(Exception e) {
				LastInstallHelperException = e;
				return false;
			}
		}

		public static bool UninstallService() {
			try {
				if (!IsInstalled) {
					ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
					return true;
				} else return false;
			} catch (Exception e) {
				LastInstallHelperException = e;
				return false;
			}
		}

		public static void ReinstallService() {
			UninstallService();
			InstallService();
		}

		public static ServiceController GetService() {
			ServiceController ret = null;
			foreach (ServiceController sc in ServiceController.GetServices()) {
				if (sc.ServiceName == ServiceName) {
					ret = sc;
					break;
				}
			}
			return ret;
		}

	}
}
