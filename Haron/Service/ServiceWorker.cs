﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace Haron.Service {
	public class ServiceWorker : ServiceBase {

		protected override void OnContinue() { 
		
		}
		protected override void OnCustomCommand(int command) { 
		
		}
		protected override void OnPause() { 
		
		}
		protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus) {
			return true;
		}
		protected override void OnSessionChange(SessionChangeDescription changeDescription) { 
		
		}
		protected override void OnShutdown() { 
		
		}
		protected override void OnStart(string[] args) { 
		
		}
		protected override void OnStop() { 
		
		}
	}
}
