﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration.Install;
using System.ServiceProcess;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections;

namespace Haron.Service {
	[RunInstaller(true)]
	public class AppServiceInstaller : Installer {
		public AppServiceInstaller() {
			ServiceProcessInstaller spi = new ServiceProcessInstaller();
			ServiceInstaller sp = new ServiceInstaller();

			spi.Account = ServiceAccount.NetworkService;
			spi.Username = null;
			spi.Password = null;

			sp.DelayedAutoStart = true;
			sp.ServiceName = ServiceSupervisor.ServiceName;
			sp.DisplayName = ServiceSupervisor.DisplayName;
			sp.Description = ServiceSupervisor.Description;
			sp.StartType = ServiceStartMode.Automatic;

			this.Installers.Add(sp);
			this.Installers.Add(spi);
		}
	}
}