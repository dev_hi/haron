﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Haron.Log {
	public class LogStream {
		private static LogStream Global = null;
		public static LogStream Error = new LogStream();
		private List<Stream> LogOutputStream = new List<Stream>();

		public static LogStream GetLogger() {
			if (LogStream.Global is null) LogStream.Global = new LogStream();
			return LogStream.Global;
		}
		public static void Log(string message, bool timestamp) {
			GetLogger().LogMessage(message, timestamp);
		}
		public static void Log(Exception e, bool stackout, bool timestamp) {
			GetLogger().LogException(e, stackout, timestamp);
		}
		public static void AddStream(Stream stream) {
			GetLogger().AddOutputStream(stream);
		}

		public LogStream() {

		}

		public void AddOutputStream(Stream stream) {
			if(this.LogOutputStream.IndexOf(stream) == -1) this.LogOutputStream.Add(stream);
		}

		public void LogMessage(string message, bool timestamp) {
			DateTime time = DateTime.Now;
			byte[] buffer = Encoding.UTF8.GetBytes((timestamp ? "[" + time.ToString() + "]" : "") + message + "\r\n");
			this.Write(buffer, 0, buffer.Length);
		}

		public void LogException(Exception e, bool stackout, bool timestamp) {
			this.LogMessage(e.ToString() + ":" + e.Message + (stackout ? 
				"\r\n#=======# callstack #=======#\r\n" + 
				e.StackTrace + 
				"\r\n#=====# callstack end #=====#" : ""), 
			timestamp);
		}
		
		public void Write(byte[] buffer, int offset, int count) {
			for (int i = 0; i < this.LogOutputStream.Count; ++i) {
				if (this.LogOutputStream[i].CanWrite) {
					this.LogOutputStream[i].Write(buffer, 0, buffer.Length);
				}
			}
		}
	}
}
