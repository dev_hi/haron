﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Markup;

namespace Haron.Presentation.Controls
{
	/// <summary>
	/// DashboardGrid.xaml에 대한 상호 작용 논리
	/// </summary>
	[ContentProperty(nameof(Children))]
	public partial class DashboardGrid : UserControl {
		public static readonly DependencyPropertyKey ChildrenProperty = DependencyProperty.RegisterReadOnly(nameof(Children), typeof(UIElementCollection), typeof(DashboardGrid), new FrameworkPropertyMetadata(new PropertyChangedCallback(PropertyChanged)));

		public const int ItemWidth = 200;
		public const int ItemHeight = 200;
		public int Columns {
			get {
				return (int)this.ActualWidth / ItemWidth;
			}
		}
		public int Rows {
			get {
				return (int)Math.Ceiling((double)this.MainGrid.Children.Count / this.Columns);
			}
		}
		public UIElementCollection Children {
			get { return (UIElementCollection)GetValue(ChildrenProperty.DependencyProperty); }
			private set { SetValue(ChildrenProperty, value); }
		}
		public DashboardGrid() {
			InitializeComponent();
			if(DesignerProperties.GetIsInDesignMode(this)) {
				Label l = new Label {
					Content = "디자인 모드에선 미리보기가 지원되지 않습니다.",
					FontSize = 30,
					Foreground = Brushes.White
				};
				this.MainGrid.Children.Add(l);
			}
		}
		private static void PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			DashboardGrid obj = (DashboardGrid)sender;

		}

		public void AddItem(UIElement item) {
			this.MainGrid.Children.Add(item);
		}

		public void AddItem(Type item) {
			this.MainGrid.Children.Add((UIElement)Activator.CreateInstance(item));
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e) {
			if(!DesignerProperties.GetIsInDesignMode(this)) {
				bool changed = false;
				while (this.Columns != (this.MainGrid.ColumnDefinitions.Count - 1)) {
					if ((this.MainGrid.ColumnDefinitions.Count - 1) < this.Columns) {
						ColumnDefinition c = new ColumnDefinition();
						c.Width = new GridLength(ItemWidth);
						this.MainGrid.ColumnDefinitions.Add(c);
					}
					else if ((this.MainGrid.ColumnDefinitions.Count - 1) > this.Columns) {
						this.MainGrid.ColumnDefinitions.RemoveAt(this.MainGrid.ColumnDefinitions.Count - 1);
					}
					changed = true;
				}
				while (this.Rows != (this.MainGrid.RowDefinitions.Count - 1)) {
					if ((this.MainGrid.RowDefinitions.Count - 1) < this.Rows) {
						RowDefinition r = new RowDefinition();
						r.Height = new GridLength(ItemHeight);
						this.MainGrid.RowDefinitions.Add(r);
					}
					else if ((this.MainGrid.RowDefinitions.Count - 1) > this.Rows) {
						this.MainGrid.RowDefinitions.RemoveAt(this.MainGrid.RowDefinitions.Count - 1);
					}
					changed = true;
				}
				if (changed) {
					for (int i = 0; i < this.MainGrid.Children.Count; ++i) {
						Grid.SetColumn(this.MainGrid.Children[i], (i % this.Columns));
						Grid.SetRow(this.MainGrid.Children[i], (i / this.Columns));
					}
				}
			}
		}
	}
}
