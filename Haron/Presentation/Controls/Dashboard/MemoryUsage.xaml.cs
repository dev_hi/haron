﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Haron.Presentation.Controls.Dashboard {
	/// <summary>
	/// MemoryUsage.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MemoryUsage : UserControl {
		private DispatcherTimer t = new DispatcherTimer();
		public const long DefaultInterval = 100000;
		public long Interval {
			get {
				return this.t.Interval.Ticks;
			}
			set {
				this.t.Interval = new TimeSpan(value);
			}
		}

		public MemoryUsage() {
			this.t.Tick += this.T_Tick;
			this.Interval = DefaultInterval;
			InitializeComponent();
		}

		private void T_Tick(object sender, EventArgs e) {
			update();
		}

		private void update() {
			/*ulong t = Usage.TotalMemoryCapacity;
			ulong a = Usage.AvailableMemory;
			ulong u = t - a;
			double b = (double)u / (double)t * 100;
			this.RAM_USAGE_LABEL.Content = (int)b + "%";
			this.RAM_TOTAL.Content = "전체 메모리 : " + l2s(t);
			this.RAM_USING.Content = "사용중 : " + l2s(u);
			this.RAM_AVAILABLE.Content = "사용가능 : " + l2s(a);*/
		}

		private string l2s(ulong l) {
			double a = (double)l;
			string ret = "";
			int m;
			for(m = 0; a >= 1024; ++m) a /= 1024;
			ret += a.ToString();
			if (ret.Length > 4) ret = ret.Substring(0, 4);
			switch (m) {
				case 0:
					ret += "Bytes";
					break;
				case 1:
					ret += "KB";
					break;
				case 2:
					ret += "MB";
					break;
				case 3:
					ret += "GB";
					break;
				case 4:
					ret += "TB";
					break;
				default:
					ret += "PB";
					break;
			}
			return ret;
		}

		public void StartMeter() {
			update();
			this.t.Start();
		}

		public void StopMeter() {
			this.t.Stop();
		}
	}
}
