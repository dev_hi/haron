﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Haron.Presentation.Controls;
using System.Windows.Threading;

namespace Haron.Presentation.Controls.Dashboard {
	/// <summary>
	/// CpuMeter.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class CpuMeter : UserControl {
		private DispatcherTimer t = new DispatcherTimer();
		public const long DefaultInterval = 10000000;
		public long Interval {
			get {
				return this.t.Interval.Ticks;
			}
			set {
				this.t.Interval = new TimeSpan(value);
			}
		}

		public CpuMeter() : base() {
			this.t.Tick += this.T_Tick;
			this.Interval = DefaultInterval;
			InitializeComponent();
		}

		private void T_Tick(object sender, EventArgs e) {
			//this.CPU_USAGE_LABEL.Content = (int)(Usage.ProcessorUsagePercent()) + "%";
		}

		public void StartMeter() {
			//this.CPU_USAGE_LABEL.Content = (int)(Usage.ProcessorUsagePercent()) + "%";
			this.t.Start();
		}

		public void StopMeter() {
			this.t.Stop();
		}
	}
}
