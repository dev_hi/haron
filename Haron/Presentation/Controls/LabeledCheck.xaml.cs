﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Haron.Presentation.Controls
{
	/// <summary>
	/// LabeledCheck.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class LabeledCheck : UserControl {
		public static readonly DependencyProperty LabelTextDependancy = DependencyProperty.Register(nameof(LabelText), typeof(string), typeof(LabeledCheck), new FrameworkPropertyMetadata(new PropertyChangedCallback(PropertyChanged)));
		public static readonly DependencyProperty CheckedDependancy = DependencyProperty.Register(nameof(Checked), typeof(bool), typeof(LabeledCheck), new FrameworkPropertyMetadata(new PropertyChangedCallback(PropertyChanged)));
		public string LabelText {
			get {
				return (string)GetValue(LabelTextDependancy);
			}
			set {
				SetValue(LabelTextDependancy, value);
			}
		}
		public bool Checked {
			get {
				return (bool)GetValue(CheckedDependancy);
			}
			set {
				SetValue(CheckedDependancy, value);
			}
		}
		public string test = "";
		public EventHandler OnCheckedChanged;

		private bool isChecked = false;

		public LabeledCheck()
        {
            InitializeComponent();
		}

		private static void PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			if (e.Property == LabelTextDependancy) {
				((LabeledCheck)sender).TextLabel.Content = e.NewValue;
			}
			if (e.Property == CheckedDependancy) {
				((LabeledCheck)sender).SetChecked((bool)e.NewValue);
			}
		}

		public void SetChecked(bool check) {
			this.isChecked = check;
			if(check) {
				this.CheckboxButton.Background = (ImageBrush)this.Resources["Button.Background.Checked"];
			}
			else {
				this.CheckboxButton.Background = (ImageBrush)this.Resources["Button.Background.Unchecked"];
			}
		}
		private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
			ToggleCheck();
		}

		private void ToggleCheck() {
			if (this.isChecked) this.SetChecked(false);
			else this.SetChecked(true);
			if(this.OnCheckedChanged != null) this.OnCheckedChanged(this, new EventArgs());
		}

		private void Grid_MouseEnter(object sender, MouseEventArgs e) {
			this.BorderBG.Background = (SolidColorBrush)this.Resources["Background.MouseOver.Color"];
		}

		private void Grid_MouseLeave(object sender, MouseEventArgs e) {
			this.BorderBG.Background = (SolidColorBrush)this.Resources["Background.Color"];
		}
	}
}
