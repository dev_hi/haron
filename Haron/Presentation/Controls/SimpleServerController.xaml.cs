﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Haron.Presentation.Controls {
	/// <summary>
	/// SimpleServerController.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class SimpleServerController : UserControl {
		public static readonly DependencyProperty ServerNamePropertyDependancy =
			DependencyProperty.Register(
				"ServerNameProperty",
				typeof(string),
				typeof(SimpleServerController),
				new FrameworkPropertyMetadata(OnTextChangePropertyChanged)
			);
		public string ServerName {
			get {
				return (string)GetValue(ServerNamePropertyDependancy);
			}
			set {
				SetValue(ServerNamePropertyDependancy, value);
			}
		}
		private static void OnTextChangePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			SimpleServerController s = (SimpleServerController)d;
			if (e.Property == ServerNamePropertyDependancy) {
				s.TextLabel.Content = e.NewValue;
			}
		}

		public SimpleServerController() {
			InitializeComponent();
		}
	}
}
