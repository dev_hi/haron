﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using Haron.Presentation.Pages;
using Haron.Presentation.Controls;
using Haron.Presentation.Controls.Dashboard;

namespace Haron.Presentation {
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window {
		public const string DefaultWindowTtile = "Haron";
		public delegate void OnPageLoadedEventDelegate(object sender, OnPageLoadedEventArgs e);
		public class OnPageLoadedEventArgs : EventArgs {
			public Page PageIncetance { get; private set; }
			public Type PageType { get { return this.PageIncetance.GetType(); } }
			public OnPageLoadedEventArgs(Page i) {
				this.PageIncetance = i;
			}
		}
		public OnPageLoadedEventDelegate OnPageLoaded;

		public Brush WindowCaptionBrush {
			get { return this.WindowCaptionGrid.Background; }
			set { this.WindowCaptionGrid.Background = value; }
		}
		public Brush WindowBodyBrush {
			get { return this.BodyDockPanel.Background; }
			set { this.BodyDockPanel.Background = value; }
		}
		public Brush ScrollMenuBrush {
			get { return this.ScrollMenu.Background; }
			set { this.ScrollMenu.Background = value; }
		}

		public MainWindow() {
			InitializeComponent();
			SubInittializeComponent();
		}
		private void SubInittializeComponent() {
			this.Title = DefaultWindowTtile;
			this.OnPageLoaded += pageloaded;
			this.AddMenuItem((BitmapImage)this.Resources["DashboardImage"], "Dashboard", typeof(Dashboard));
			this.AddMenuItem((BitmapImage)this.Resources["ServerImage"], "Servers", typeof(Servers));
			this.AddMenuItem((BitmapImage)this.Resources["RouteImage"], "HTTP Routing", typeof(Routing));
			this.AddMenuItem((BitmapImage)this.Resources["ExtensionImage"], "Plugins", typeof(Plugins));
			this.AddMenuItem((BitmapImage)this.Resources["LogsImage"], "Logs", typeof(Logs));
		}

		private void pageloaded(object sender, OnPageLoadedEventArgs e) {
			if(e.PageType.Equals(typeof(Dashboard))) {
				Dashboard page = (Dashboard)e.PageIncetance;
				CpuMeter m = new CpuMeter();
				page.MainGrid.AddItem(m);
				m.StartMeter();
				MemoryUsage m2 = new MemoryUsage();
				m2.SetValue(Grid.ColumnSpanProperty, 2);
				page.MainGrid.AddItem(m2);
				m2.StartMeter();
			}
		}

		#region WndCaption
		bool WindowWillMove = false;
		private void ToggleMaximize() {
			if (this.WindowState == WindowState.Maximized) {
				this.WindowState = WindowState.Normal;
				this.wnd_maxi_btn_img.Source = (BitmapImage)this.Resources["MaximizeButtonImage"];
			}
			else if (this.WindowState == WindowState.Normal) {
				this.WindowState = WindowState.Maximized;
				this.wnd_maxi_btn_img.Source = (BitmapImage)this.Resources["NormalizeButtonImage"];
			}
		}
		private void Wnd_close_btn_Click(object sender, RoutedEventArgs e) {
			this.Close();
		}
		private void Wnd_max_btn_Click(object sender, RoutedEventArgs e) {
			ToggleMaximize();
		}
		private void Wnd_closebtn_Click(object sender, RoutedEventArgs e) {
			WindowState = WindowState.Minimized;
		}
		private void WindowCaptionGrid_MouseDown(object sender, MouseButtonEventArgs e) {
			if (e.ClickCount == 2) {
				ToggleMaximize();
			}
			if (e.ChangedButton == MouseButton.Left) {
				WindowWillMove = true;
				this.DragMove();
			}
		}
		private void WindowCaptionGrid_MouseMove(object sender, MouseEventArgs e) {
			if (WindowWillMove && e.LeftButton == MouseButtonState.Pressed && (this.WindowState == WindowState.Maximized)) {
				ToggleMaximize();
				Point t = e.GetPosition(this);
				this.Left = t.X - this.Left;
				this.Top = t.Y - 20;
				this.DragMove();
			}
		}
		private void WindowCaptionGrid_MouseUp(object sender, MouseButtonEventArgs e) {
			WindowWillMove = false;
		}
		private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
			if (this.WindowState == WindowState.Maximized) this.wnd_maxi_btn_img.Source = (BitmapImage)this.Resources["NormalizeButtonImage"];
		}
		#endregion

		#region MenuAndPage
		private Dictionary<Button, Type> PageClasses = new Dictionary<Button, Type>();
		public void AddMenuItem(BitmapImage img, string text, Type pageclass) {
			if (pageclass.BaseType != typeof(Page)) throw new ArgumentException("페이지 클래스 타입 인수가 전달되지 않았습니다.");
			Button btn = CreateDefaultMenuButton(img, text);
			btn.Tag = this.PageClasses.Count;
			this.PageClasses.Add(btn, pageclass);
			btn.Click += GeneralMenuItemOnClick;
			ScrollMenu.Children.Add(btn);
		}
		private void GeneralMenuItemOnClick(object sender, RoutedEventArgs e) {
			Type cl = this.PageClasses[(Button)sender];
			Page i = (Page)Activator.CreateInstance(cl);
			this.ChangePage(i);
			OnPageLoaded(sender, new OnPageLoadedEventArgs(i));
		}
		public Button CreateDefaultMenuButton(BitmapImage img, string text) {
			Button btn = new Button();
			btn.HorizontalContentAlignment = HorizontalAlignment.Left;
			btn.Style = (Style)this.Resources["MenuButtonStyle"];
			btn.Height = 60;
			btn.BorderThickness = new Thickness(0);
			StackPanel sp = new StackPanel();
			sp.Margin = new Thickness(10);
			sp.Orientation = Orientation.Horizontal;
			Image imgui = new Image();
			imgui.Source = img;
			imgui.Width = 40;
			Label label = new Label();
			label.Foreground = btn.Foreground;
			label.Content = text;
			sp.Children.Add(imgui);
			sp.Children.Add(label);
			btn.Content = sp;
			return btn;
		}
		private void ChangePage(Page p) {
			body_contents.Navigate(p);
			this.Title = DefaultWindowTtile + " " + p.Title;
		}
		#endregion

	}
}
