﻿using Haron.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haron.Presentation.TeamProject {
	public interface IServerControl {
		IServer GetServer();
		void StartServer();
		void StopServer();
	}
}
