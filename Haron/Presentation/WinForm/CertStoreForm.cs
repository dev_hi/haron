﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haron.Presentation.TeamProject {
	public partial class CertStoreForm : Form {
		X509Store _store;
		public int Index {
			get {
				return this.listView1.SelectedIndices[0];
			}
		}

		public CertStoreForm(X509Store store) {
			this._store = store;
			InitializeComponent();
		}

		private void CertStoreForm_Load(object sender, EventArgs e) {
			for(int i = 0; i < this._store.Certificates.Count; ++i) {
				string ext = "";
				for(int j = 0; j < this._store.Certificates[i].Extensions.Count; ++j) {
					ext += this._store.Certificates[i].Extensions[j].Format(false);
					if(j < this._store.Certificates[i].Extensions.Count - 1) ext += ", ";
				}
				listView1.Items.Add(new ListViewItem(new string[] {
					this._store.Certificates[i].Subject,
					this._store.Certificates[i].Issuer,
					this._store.Certificates[i].NotBefore.ToString(),
					ext
				}));
			}
		}

		private void button1_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
		}

		private void listView1_MouseDoubleClick(object sender, MouseEventArgs e) {
			if(this.listView1.SelectedIndices.Count == 1) this.DialogResult = DialogResult.OK;
		}
	}
}
