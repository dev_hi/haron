﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Haron.Plugin;

namespace Haron.Presentation.TeamProject {
	public partial class PluginList : Form {
		public PluginList() {
			InitializeComponent();
		}

		private void PluginList_Load(object sender, EventArgs e) {
			for(int i = 0; i < PluginManager.LoadedPlugin.Count; ++i) {
				ListViewItem item = new ListViewItem(i.ToString());
				if (PluginManager.LoadedPlugin[i].LoadSuccess) {
					item.SubItems.Add(PluginManager.LoadedPlugin[i].GUID.ToString());
					item.SubItems.Add(PluginManager.LoadedPlugin[i].Name);
					item.SubItems.Add(PluginManager.LoadedPlugin[i].Author);
					item.SubItems.Add(PluginManager.LoadedPlugin[i].DLL);
					item.SubItems.Add(PluginManager.LoadedPlugin[i].Description);
				}
				else {
					item.SubItems.Add("로드 실패");
					item.SubItems.Add("로드 실패");
					item.SubItems.Add("로드 실패");
					item.SubItems.Add(PluginManager.LoadedPlugin[i].DLL);
					item.SubItems.Add("로드 실패");
				}
				item.SubItems.Add(PluginManager.LoadedPlugin[i].LoadSuccess ? "예" : "아니오");
				this.listView1.Items.Add(item);
			}
		}
	}
}
