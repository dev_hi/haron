﻿namespace Haron.Presentation.TeamProject {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.서버추가ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.hTTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.플러그인ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.전체중단ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.서버추가ToolStripMenuItem,
            this.플러그인ToolStripMenuItem,
            this.전체중단ToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1268, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// 서버추가ToolStripMenuItem
			// 
			this.서버추가ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hTTPToolStripMenuItem});
			this.서버추가ToolStripMenuItem.Name = "서버추가ToolStripMenuItem";
			this.서버추가ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
			this.서버추가ToolStripMenuItem.Text = "서버추가";
			this.서버추가ToolStripMenuItem.Click += new System.EventHandler(this.서버추가ToolStripMenuItem_Click);
			// 
			// hTTPToolStripMenuItem
			// 
			this.hTTPToolStripMenuItem.Name = "hTTPToolStripMenuItem";
			this.hTTPToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
			this.hTTPToolStripMenuItem.Text = "HTTP";
			this.hTTPToolStripMenuItem.Click += new System.EventHandler(this.hTTPToolStripMenuItem_Click);
			// 
			// 플러그인ToolStripMenuItem
			// 
			this.플러그인ToolStripMenuItem.Name = "플러그인ToolStripMenuItem";
			this.플러그인ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
			this.플러그인ToolStripMenuItem.Text = "플러그인";
			this.플러그인ToolStripMenuItem.Click += new System.EventHandler(this.플러그인ToolStripMenuItem_Click);
			// 
			// 전체중단ToolStripMenuItem
			// 
			this.전체중단ToolStripMenuItem.Name = "전체중단ToolStripMenuItem";
			this.전체중단ToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
			this.전체중단ToolStripMenuItem.Text = "전체 중단";
			this.전체중단ToolStripMenuItem.Click += new System.EventHandler(this.전체중단ToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1268, 792);
			this.Controls.Add(this.menuStrip1);
			this.IsMdiContainer = true;
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "SimpleServer";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 서버추가ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem hTTPToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 플러그인ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 전체중단ToolStripMenuItem;
	}
}