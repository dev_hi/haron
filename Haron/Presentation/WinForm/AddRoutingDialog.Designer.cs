﻿namespace Haron.Presentation.TeamProject {
	partial class AddRoutingDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.uritext = new System.Windows.Forms.TextBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.middlelist = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.addedmiddle = new System.Windows.Forms.ListView();
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.middleadd = new System.Windows.Forms.Button();
			this.middlerem = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.addedusers = new System.Windows.Forms.ListView();
			this.userlist = new System.Windows.Forms.ListView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.userrem = new System.Windows.Forms.Button();
			this.useradd = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// uritext
			// 
			this.uritext.Location = new System.Drawing.Point(42, 12);
			this.uritext.Name = "uritext";
			this.uritext.Size = new System.Drawing.Size(503, 21);
			this.uritext.TabIndex = 0;
			this.uritext.Text = "/";
			this.uritext.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(16, 20);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(76, 16);
			this.checkBox1.TabIndex = 1;
			this.checkBox1.Text = "인증 필요";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// middlelist
			// 
			this.middlelist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader5});
			this.middlelist.FullRowSelect = true;
			this.middlelist.Location = new System.Drawing.Point(6, 20);
			this.middlelist.Name = "middlelist";
			this.middlelist.Size = new System.Drawing.Size(208, 237);
			this.middlelist.TabIndex = 2;
			this.middlelist.UseCompatibleStateImageBehavior = false;
			this.middlelist.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "이름";
			// 
			// addedmiddle
			// 
			this.addedmiddle.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader6});
			this.addedmiddle.FullRowSelect = true;
			this.addedmiddle.Location = new System.Drawing.Point(319, 20);
			this.addedmiddle.Name = "addedmiddle";
			this.addedmiddle.Size = new System.Drawing.Size(208, 237);
			this.addedmiddle.TabIndex = 3;
			this.addedmiddle.UseCompatibleStateImageBehavior = false;
			this.addedmiddle.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "이름";
			// 
			// middleadd
			// 
			this.middleadd.Location = new System.Drawing.Point(220, 20);
			this.middleadd.Name = "middleadd";
			this.middleadd.Size = new System.Drawing.Size(93, 52);
			this.middleadd.TabIndex = 4;
			this.middleadd.Text = "추가 >>";
			this.middleadd.UseVisualStyleBackColor = true;
			this.middleadd.Click += new System.EventHandler(this.middleadd_Click);
			// 
			// middlerem
			// 
			this.middlerem.Location = new System.Drawing.Point(220, 205);
			this.middlerem.Name = "middlerem";
			this.middlerem.Size = new System.Drawing.Size(93, 52);
			this.middlerem.TabIndex = 5;
			this.middlerem.Text = "<< 삭제";
			this.middlerem.UseVisualStyleBackColor = true;
			this.middlerem.Click += new System.EventHandler(this.middlerem_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(24, 12);
			this.label1.TabIndex = 6;
			this.label1.Text = "URI";
			// 
			// addedusers
			// 
			this.addedusers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4});
			this.addedusers.Enabled = false;
			this.addedusers.FullRowSelect = true;
			this.addedusers.Location = new System.Drawing.Point(319, 45);
			this.addedusers.Name = "addedusers";
			this.addedusers.Size = new System.Drawing.Size(208, 201);
			this.addedusers.TabIndex = 7;
			this.addedusers.UseCompatibleStateImageBehavior = false;
			this.addedusers.View = System.Windows.Forms.View.Details;
			// 
			// userlist
			// 
			this.userlist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
			this.userlist.Enabled = false;
			this.userlist.FullRowSelect = true;
			this.userlist.Location = new System.Drawing.Point(6, 45);
			this.userlist.Name = "userlist";
			this.userlist.Size = new System.Drawing.Size(208, 201);
			this.userlist.TabIndex = 8;
			this.userlist.UseCompatibleStateImageBehavior = false;
			this.userlist.View = System.Windows.Forms.View.Details;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.userrem);
			this.groupBox1.Controls.Add(this.useradd);
			this.groupBox1.Controls.Add(this.userlist);
			this.groupBox1.Controls.Add(this.addedusers);
			this.groupBox1.Controls.Add(this.checkBox1);
			this.groupBox1.Location = new System.Drawing.Point(12, 39);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(533, 252);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "인증";
			// 
			// userrem
			// 
			this.userrem.Enabled = false;
			this.userrem.Location = new System.Drawing.Point(220, 194);
			this.userrem.Name = "userrem";
			this.userrem.Size = new System.Drawing.Size(93, 52);
			this.userrem.TabIndex = 10;
			this.userrem.Text = "<< 삭제";
			this.userrem.UseVisualStyleBackColor = true;
			this.userrem.Click += new System.EventHandler(this.userrem_Click);
			// 
			// useradd
			// 
			this.useradd.Enabled = false;
			this.useradd.Location = new System.Drawing.Point(220, 45);
			this.useradd.Name = "useradd";
			this.useradd.Size = new System.Drawing.Size(93, 52);
			this.useradd.TabIndex = 9;
			this.useradd.Text = "추가 >>";
			this.useradd.UseVisualStyleBackColor = true;
			this.useradd.Click += new System.EventHandler(this.useradd_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.middlerem);
			this.groupBox2.Controls.Add(this.middlelist);
			this.groupBox2.Controls.Add(this.middleadd);
			this.groupBox2.Controls.Add(this.addedmiddle);
			this.groupBox2.Location = new System.Drawing.Point(12, 297);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(533, 263);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "미들웨어";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(389, 566);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 11;
			this.button1.Text = "확인";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(470, 566);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 12;
			this.button2.Text = "취소";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(308, 566);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 13;
			this.button3.Text = "삭제";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Visible = false;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "이름";
			this.columnHeader2.Width = 179;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "이름";
			this.columnHeader4.Width = 195;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "설명";
			this.columnHeader5.Width = 118;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "설명";
			this.columnHeader6.Width = 127;
			// 
			// AddRoutingDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(557, 601);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.uritext);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "AddRoutingDialog";
			this.Text = "라우팅";
			this.Load += new System.EventHandler(this.AddRoutingDialog_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox uritext;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.ListView middlelist;
		private System.Windows.Forms.ListView addedmiddle;
		private System.Windows.Forms.Button middleadd;
		private System.Windows.Forms.Button middlerem;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListView addedusers;
		private System.Windows.Forms.ListView userlist;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button userrem;
		private System.Windows.Forms.Button useradd;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
	}
}