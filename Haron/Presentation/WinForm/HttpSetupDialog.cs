﻿using Haron.Server.Http;
using Haron.Server.Http.MiddleWare;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Haron.Authorization;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Haron.Plugin;

namespace Haron.Presentation.TeamProject {
	public partial class HttpSetupDialog : Form {
		//public static List<Type> MiddleWareTypes = new List<Type>();
		public Dictionary<string, string> Users {
			get {
				Dictionary<string, string> ret = new Dictionary<string, string>();
				for(int i = 0; i < this.Userslist.Items.Count; ++i) {
					ret.Add(this.Userslist.Items[i].Text, (string)this.Userslist.Items[i].Tag);
				}
				return ret;
			}
		}

		RouterNode root = new RouterNode();
		X509Certificate2 cert;

		public HttpSetupInformation GetResultSetupInfo() {
			HttpSetupInformation info = new HttpSetupInformation();
			try {
				if(this.modifying) {
					info.HttpRoutingRoot = this.root;
					info.AuthenticationDictionary = this.Users;
				}
				else {
					info.HttpRoutingRoot = this.root;
					info.ServerName = textBox1.Text;
					info.Backlog = int.Parse(textBox4.Text);
					info.BindEndPoint = new IPEndPoint((IPAddress)this.comboBox1.SelectedItem, int.Parse(this.textBox2.Text));
					info.AuthenticationDictionary = this.Users;
					info.MaxAlive = int.Parse(textBox3.Text);
					info.Certificate = this.checkBox1.Checked ? this.cert : null;
				}
			}
			catch (Exception e) {
				MessageBox.Show(e.Message);
			}
			return info;
		}

		public HttpSetupDialog() {
			InitializeComponent();
			certdlg.Filter = "PKCS#12 인증서|*.pfx";
			IPAddress[] addrs = Dns.GetHostAddresses(Dns.GetHostName());
			this.comboBox1.Items.Add(IPAddress.Any);
			this.comboBox1.Items.Add(IPAddress.Loopback);
			for (int i = 0; i < addrs.Length; ++i) {
				if (!addrs[i].IsIPv4MappedToIPv6 && !addrs[i].IsIPv6LinkLocal) this.comboBox1.Items.Add(addrs[i]);
			}
			this.comboBox1.SelectedIndex = 0;
		}

		private bool modifying = false;

		public HttpSetupDialog(HttpServer server) : this() {
			this.modifying = true;
			this.textBox1.Text = server.ServerName;
			this.textBox2.Text = server.Port.ToString();
			this.textBox3.Enabled = false;
			this.textBox4.Text = server.Backlog.ToString();
			this.comboBox1.SelectedItem = server.BindAddress;
			this.root = server.Router;
			this.textBox1.Enabled = false;
			this.textBox2.Enabled = false;
			this.textBox3.Enabled = false;
			this.textBox4.Enabled = false;
			this.comboBox1.Enabled = false;
			foreach(KeyValuePair<string, string> kv in server.Authusers) {
				ListViewItem item = new ListViewItem(kv.Key);
				item.Tag = kv.Value;
				this.Userslist.Items.Add(item);
			}
			this.checkBox1.Enabled = false;
			if(server.Secured) {
				this.textBox5.TextAlign = HorizontalAlignment.Center;
				this.textBox5.Text = "인증서 위치는 저장되지 않습니다.";
				this.textBox6.Text = "************";
			}
		}

		public static bool CheckTcpPort(IPAddress ip, int port) {
			bool ret = false;
			try {
				Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				sock.Bind(new IPEndPoint(ip, port));
				sock.Close();
				ret = true;
			}
			catch(SocketException ex) { }
			return ret;
		}

		private void button1_Click(object sender, EventArgs e) {
			if(this.modifying) {
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
			else {
				if (CheckTcpPort((IPAddress)this.comboBox1.SelectedItem, int.Parse(this.textBox2.Text))) {
					try {
						if(this.checkBox1.Checked) {
							if (this.SelectedFromStore) {
								this.cert = new X509Certificate2(this.cert.RawData, this.textBox6.Text);
							}
							else {
								this.cert = new X509Certificate2(this.textBox5.Text, this.textBox6.Text);
							}
						}
						this.DialogResult = DialogResult.OK;
						this.Close();
					}
					catch (Exception ex) { MessageBox.Show("인증서 오류 : " + ex.Message); }
				}
				else {
					MessageBox.Show("해당 포트가 이미 사용중입니다.");
				}
			}
		}

		private void button2_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void button4_Click(object sender, EventArgs e) {
			AddRoutingDialog dlg;
			Type[] middlewares = MiddlewareManager.GetAllMiddlewareTypes();
			if(this.treeView1.SelectedNode != null) dlg = new AddRoutingDialog(((RouterNode)this.treeView1.SelectedNode.Tag).GetAllUriPath(), this.Users, middlewares);
			else dlg = new AddRoutingDialog(this.Users, middlewares);
			if (dlg.ShowDialog() == DialogResult.OK) {
				dlg.AppendNode(this.root);
				this.BuildRouteTree(this.root);
			}
		}

		private void BuildRouteTree(RouterNode node) {
			this.treeView1.Nodes.Clear();
			TreeNode root = new TreeNode(node.ToString());
			root.Tag = node;
			this.treeView1.Nodes.Add(root);
			for (int i = 0; i < node.ChildCount; ++i) {
				this.BuildRouteTree(root, node.GetChild(i));
			}
		}
		private void BuildRouteTree(TreeNode parent, RouterNode node) {
			TreeNode c = new TreeNode(node.ToString());
			c.Tag = node;
			parent.Nodes.Add(c);
			for (int i = 0; i < node.ChildCount; ++i) {
				this.BuildRouteTree(c, node.GetChild(i));
			}
		}

		private void button3_Click(object sender, EventArgs e) {
			UserAddDialog dlg = new UserAddDialog();
			if (dlg.ShowDialog() == DialogResult.OK) {
				bool found = false;
				for (int i = 0; i < this.Userslist.Items.Count; ++i) found = found || (this.Userslist.Items[i].Text == dlg.Username);
				if (found) {
					MessageBox.Show("이름이 중복되었습니다.");
				}
				else {
					ListViewItem item = new ListViewItem(dlg.Username);
					item.Tag = dlg.Password;
					this.Userslist.Items.Add(item);
				}
			}
		}

		private void HttpSetupDialog_Load(object sender, EventArgs e) {
			if(!this.modifying) this.root.RegisterMiddleWare("./", new Default());
			this.BuildRouteTree(this.root);
			this.treeView1.SelectedNode = this.treeView1.Nodes[0];
			this.treeView1.ContextMenu = new ContextMenu(new MenuItem[] {
					new MenuItem("수정", this.TreeContextMenuHandler),
					new MenuItem("삭제", this.TreeContextMenuHandler)
			});
			this.Userslist.ContextMenu = new ContextMenu(new MenuItem[] {
					new MenuItem("삭제", this.UserlistContextMenuHandler)
			});
		}
		
		private void UserlistContextMenuHandler(object sender, EventArgs e) {
			if (this.Userslist.SelectedItems.Count > 0) {
				this.Userslist.Items.Remove(this.Userslist.SelectedItems[0]);
			}
		}

		private void TreeContextMenuHandler(object sender, EventArgs e) {
			if (this.treeView1.SelectedNode != null) {
				MenuItem item = (MenuItem)sender;
				RouterNode node = (RouterNode)this.treeView1.SelectedNode.Tag;
				if (item.Text == "수정") {
					Type[] middlewares = MiddlewareManager.GetAllMiddlewareTypes();
					AddRoutingDialog dlg = new AddRoutingDialog(node, this.Users, middlewares);
					DialogResult r = dlg.ShowDialog();
					if (r == DialogResult.OK) {
						if (!node.IsRoot) node.Remove();
						dlg.AppendNode(this.root);
					}
					else if (r == DialogResult.Abort) {
						if (node.IsRoot) MessageBox.Show("루트 노드는 삭제가 불가능합니다.");
						else node.Remove();
					}
				}
				else if (item.Text == "삭제") {
					if (node.IsRoot) MessageBox.Show("루트 노드는 삭제가 불가능합니다.");
					else node.Remove();
				}
				this.BuildRouteTree(this.root);
			}
		}
		
		OpenFileDialog certdlg = new OpenFileDialog();
		

		private void button5_Click(object sender, EventArgs e) {
			if(this.certdlg.ShowDialog() == DialogResult.OK) {
				this.textBox5.Text = this.certdlg.FileName;
				this.SelectedFromStore = false;
				this.textBox5.Enabled = true;
				this.textBox5.TextAlign = HorizontalAlignment.Left;
			}
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {
			this.textBox5.Enabled = this.checkBox1.Checked;
			this.textBox6.Enabled = this.checkBox1.Checked;
			this.button5.Enabled = this.checkBox1.Checked;
			this.button6.Enabled = this.checkBox1.Checked;
			if (this.textBox2.Text == "80" && this.checkBox1.Checked) this.textBox2.Text = "443";
		}

		bool SelectedFromStore = false;

		private void button6_Click(object sender, EventArgs e) {
			X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadOnly);
			CertStoreForm dlg = new CertStoreForm(store);
			if(dlg.ShowDialog() == DialogResult.OK) {
				this.cert = store.Certificates[dlg.Index];
				
				this.SelectedFromStore = true;
				this.textBox5.Text = "인증서가 저장소에 있음";
				this.textBox5.Enabled = false;
				this.textBox5.TextAlign = HorizontalAlignment.Center;
			}
		}
	}
}
