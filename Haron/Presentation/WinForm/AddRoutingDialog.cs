﻿using Haron.Authorization;
using Haron.Server.Http.MiddleWare;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haron.Presentation.TeamProject {
	public partial class AddRoutingDialog : Form {

		public bool modifying = false;
		RouterNode node;
		Dictionary<string,string> UserPassDict;

		public Dictionary<string, string> addeduserL {
			get {
				Dictionary<string,string> ret = new Dictionary<string, string>();
				for (int i = 0; i < this.addedusers.Items.Count; ++i) {
					ret.Add((string)this.addedusers.Items[i].Text, (string)this.addedusers.Items[i].Tag);
				}
				return ret;
			}
		}
		public IMiddleWare[] addedmiddleL {
			get {
				IMiddleWare[] ret = new IMiddleWare[this.addedmiddle.Items.Count];
				for (int i = 0; i < ret.Length; ++i) {
					ret[i] = (IMiddleWare)Activator.CreateInstance((Type)this.addedmiddle.Items[i].Tag);
				}
				return ret;
			}
		}
		public string URI {
			get {
				return this.uritext.Text;
			}
		}
		public bool NeedAuth {
			get {
				return this.checkBox1.Checked;
			}
		}

		public AddRoutingDialog() {
			InitializeComponent();
		}

		public AddRoutingDialog(Dictionary<string, string> users, Type[] middlewares) : this() {
			this.UserPassDict = users;
			foreach (KeyValuePair<string, string> kv in this.UserPassDict) {
				ListViewItem item = new ListViewItem(kv.Key);
				item.Tag = kv.Value;
				this.userlist.Items.Add(item);
			}
			for (int i = 0; i < middlewares.Length; ++i) {
				ListViewItem item = new ListViewItem(middlewares[i].Name);
				IMiddleWare tmp = (IMiddleWare)Activator.CreateInstance(middlewares[i]);
				item.SubItems.Add(tmp.GetDescription());
				item.Tag = middlewares[i];
				this.middlelist.Items.Add(item);
			}
		}
		public AddRoutingDialog(string iuri, Dictionary<string, string> users, Type[] middlewares) : this(users, middlewares) {
			this.uritext.Text = iuri;
		}

		public AddRoutingDialog(RouterNode node, Dictionary<string, string> users, Type[] middlewares) : this() {
			this.modifying = true;
			this.button3.Visible = true;
			this.uritext.Text = node.GetAllUriPath();
			for (int i = 0; i < middlewares.Length; ++i) {
				ListViewItem item = new ListViewItem(middlewares[i].Name);
				IMiddleWare tmp = (IMiddleWare)Activator.CreateInstance(middlewares[i]);
				item.SubItems.Add(tmp.GetDescription());
				item.Tag = middlewares[i];
				if (node.GetMiddleWare(middlewares[i]) == null) this.middlelist.Items.Add(item);
				else this.addedmiddle.Items.Add(item);
			}
			HttpAuth auth = ((HttpAuth)node.GetMiddleWare(typeof(HttpAuth)));
			if(auth == null) {
				this.checkBox1.Checked = false;
				foreach (KeyValuePair<string, string> kv in users) {
					ListViewItem item = new ListViewItem(kv.Key);
					item.Tag = kv.Value;
					this.userlist.Items.Add(item);
				}
			}
			else {
				DictionaryStoreAuth auth2 = (DictionaryStoreAuth)auth.GetAuthenticationMethod();
				foreach (KeyValuePair<string, string> kv in users) {
					ListViewItem item = new ListViewItem(kv.Key);
					item.Tag = kv.Value;
					if (auth2.UserExists(kv.Key)) this.addedusers.Items.Add(item);
					else this.userlist.Items.Add(item);
				}
				this.checkBox1.Checked = true;
				this.useradd.Enabled = true;
				this.addedusers.Enabled = true;
				this.userlist.Enabled = true;
				this.userrem.Enabled = true;
			}
		}

		public RouterNode AppendNode(RouterNode parent) {
			RouterNode ret = parent.CreateNode(this.URI);
			if (ret.IsRoot) ret.RegisteredMiddleWare.Clear();
			if (this.NeedAuth) {
				ret.RegisterMiddleWare("./", new HttpAuth(new DictionaryStoreAuth(this.addeduserL)), 0);
			}
			IMiddleWare[] m = this.addedmiddleL;
			for(int i = 0; i < m.Length; ++i) {
				ret.RegisterMiddleWare("./", m[i]);
			}
			return this.node;
		}

		private void AddRoutingDialog_Load(object sender, EventArgs e) {
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {
			if(this.checkBox1.Checked) {
				this.useradd.Enabled = true;
				this.addedusers.Enabled = true;
				this.userlist.Enabled = true;
				this.userrem.Enabled = true;
			}
			else {
				this.useradd.Enabled = false;
				this.addedusers.Enabled = false;
				this.userlist.Enabled = false;
				this.userrem.Enabled = false;
			}
		}

		private void useradd_Click(object sender, EventArgs e) {
			if(this.userlist.SelectedItems.Count > 0) {
				for (int i = 0; i < this.userlist.SelectedItems.Count; ++i) {
					ListViewItem item = this.userlist.SelectedItems[i];
					this.userlist.Items.Remove(item);
					this.addedusers.Items.Add(item);
				}
			}
		}

		private void userrem_Click(object sender, EventArgs e) {
			if (this.addedusers.SelectedItems.Count > 0) {
				for (int i = 0; i < this.addedusers.SelectedItems.Count; ++i) {
					ListViewItem item = this.addedusers.SelectedItems[i];
					this.addedusers.Items.Remove(item);
					this.userlist.Items.Add(item);
				}
			}
		}

		private void middleadd_Click(object sender, EventArgs e) {
			if (this.middlelist.SelectedItems.Count > 0) {
				for(int i = 0; i < this.middlelist.SelectedItems.Count; ++i) {
					ListViewItem item = this.middlelist.SelectedItems[i];
					this.middlelist.Items.Remove(item);
					this.addedmiddle.Items.Add(item);
				}
			}
		}

		private void middlerem_Click(object sender, EventArgs e) {
			if (this.addedmiddle.SelectedItems.Count > 0) {
				for (int i = 0; i < this.addedmiddle.SelectedItems.Count; ++i) {
					ListViewItem item = this.addedmiddle.SelectedItems[i];
					this.addedmiddle.Items.Remove(item);
					this.middlelist.Items.Add(item);
				}
			}
		}

		private void textBox1_TextChanged(object sender, EventArgs e) {

		}

		private void button1_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void button2_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void button3_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Abort;
			this.Close();
		}
	}
}
