﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haron.Presentation.TeamProject {
	public partial class UserAddDialog : Form {

		public string Username {
			get {
				return this.textBox1.Text;
			}
		}

		public string Password {
			get {
				return this.textBox2.Text;
			}
		}

		public UserAddDialog() {
			InitializeComponent();
		}

		public UserAddDialog(string name) {
			InitializeComponent();
			this.textBox1.Text = name;
		}

		private void button1_Click(object sender, EventArgs e) {
			if (this.textBox1.Text.Length == 0) MessageBox.Show("유저 이름은 비울수 없습니다.");
			else {
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void button2_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
	}
}
