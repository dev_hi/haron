﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Haron.Server;
using Haron.Server.Http;
using Haron.Server.Http.Packet;

namespace Haron.Presentation.TeamProject {
	public partial class HttpControlForm : Form, IServerControl {
		public HttpServer Server;

		public HttpControlForm(HttpSetupInformation info) {
			InitializeComponent();
			this.Server = new HttpServer(info);
			this.Server.OnRequest += OnRequestCallback;
			this.Server.OnResponse += OnResponseCallback;
			this.Server.OnException += OnException;
		}

		private void OnException(Exception e) {
			this.WriteText(e.Message);
		}

		delegate void WriteTextDelegate(string text);
		private void WriteText(string text) {
			if (this.InvokeRequired) {
				this.Invoke(new WriteTextDelegate(WriteText), new object[] { text });
			}
			else {
				this.richTextBox1.Text += DateTime.Now.ToString("[HH:mm:ss] ") + text + "\n";
				this.richTextBox1.Select(this.richTextBox1.Text.Length - 1, 0);
				this.richTextBox1.ScrollToCaret();
			}
		}

		delegate void AliveDelegate(int a, int b);
		private void Alive(int a, int b) {
			if (this.InvokeRequired) {
				this.Invoke(new AliveDelegate(Alive), new object[] { a, b });
			}
			else {
				this.label2.Text = "연결 소켓 수\n" + a + " / " + b;
			}
		}

		public void OnRequestCallback(HttpRequest req) {
			this.WriteText(req.RemoteAddress + ":" + req.RemotePort + " - " + req.URI + " 요청");
		}

		public void OnResponseCallback(HttpRequest req, HttpResponse res) {
			this.WriteText(req.RemoteAddress + ":" + req.RemotePort + " - " + res.StatusCode + " 응답");
		}

		public IServer GetServer() {
			return this.Server;
		}

		public void StartServer() {
			if(!this.Server.Running) this.Server.Start();
			if(this.Server.Running) button1.Text = "서버 중지";
			else {
				this.Server.Stop();
				button1.Text = "서버 시작";
			}
		}

		public void StopServer() {
			if(this.Server.Running) this.Server.Stop();
			button1.Text = "서버 시작";
		}

		private void button1_Click(object sender, EventArgs e) {
			if (this.Server.Running) {
				this.StopServer();
			}
			else {
				this.StartServer();
			}
		}

		private void button2_Click(object sender, EventArgs e) {
			HttpSetupDialog dlg = new HttpSetupDialog(this.Server);
			if(dlg.ShowDialog() == DialogResult.OK) {
				this.Server.ChangeServerSettings(dlg.GetResultSetupInfo());
			}
		}

		private void Destroy() {
			if (this.Server.Running) {
				this.StopServer();
			}
		}

		private void HttpControlForm_FormClosed(object sender, FormClosedEventArgs e) {
			this.Destroy();
		}
		Timer t = new Timer();

		private void HttpControlForm_Load(object sender, EventArgs e) {
			this.Text = this.Server.ServerName;
			this.label1.Text = this.Server.BindAddress + " 바인드\n" + this.Server.Port + "포트 사용중";
			t.Tick += this.T_Tick;
			t.Interval = 500;
			t.Start();
		}

		private void T_Tick(object sender, EventArgs e) {
			Alive(this.Server.CurrentAlive, this.Server.MaxAlive);
		}
	}
}
