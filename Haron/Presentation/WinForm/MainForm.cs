﻿using Haron.Server.Http.MiddleWare;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haron.Presentation.TeamProject {
	public partial class MainForm : Form {

		List<IServerControl> ControlChildren = new List<IServerControl>();

		public MainForm() {
			InitializeComponent();
		}

		public static void AddServerMenu() {

		}

		private void hTTPToolStripMenuItem_Click(object sender, EventArgs e) {
			HttpSetupDialog dlg = new HttpSetupDialog();
			if (dlg.ShowDialog() == DialogResult.OK) {
				HttpControlForm form = new HttpControlForm(dlg.GetResultSetupInfo());
				form.MdiParent = this;
				form.Show();
				this.ControlChildren.Add(form);
			}
		}

		private void 서버추가ToolStripMenuItem_Click(object sender, EventArgs e) {
		}

		private void 플러그인ToolStripMenuItem_Click(object sender, EventArgs e) {
			PluginList form = new PluginList();
			form.ShowDialog();
		}

		private void 전체중단ToolStripMenuItem_Click(object sender, EventArgs e) {
			for(int i = 0; i < this.ControlChildren.Count; ++i) {
				this.ControlChildren[i].StopServer();
			}
		}
	}
}
