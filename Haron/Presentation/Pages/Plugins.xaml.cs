﻿using Haron.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Haron.Presentation.Pages
{
    /// <summary>
    /// Plugins.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Plugins : Page
    {
        public Plugins()
        {
            InitializeComponent();
			foreach(PluginLoader i in PluginManager.LoadedPlugin) {
				string str = "";
				if (i.LoadSuccess) {
					str = "제목 : " + i.Name + "\n";
					str += "제작자 : " + i.Author + "\n";
					str += "GUID : " + i.GUID + "\n";
					str += "버전 : " + i.Version + "\n";
					str += "설명 : " + i.Description;
				}
				else {
					str = "NOT LOADED CORRECTLY";
				}

				Label label = new Label();
				label.Content = str;
				label.Foreground = Brushes.White;
				label.Padding = new Thickness(5);
				stack_test.Children.Add(label);
			}
		}
    }
}
