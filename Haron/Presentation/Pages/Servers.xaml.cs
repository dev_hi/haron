﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Haron.Presentation.Controls;

namespace Haron.Presentation.Pages
{
    /// <summary>
    /// Servers.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Servers : Page
    {
        public Servers()
        {
            InitializeComponent();
        }

		private int i = 0;

		private void Button_Click(object sender, RoutedEventArgs e) {
			SimpleServerController ssc = new SimpleServerController();
			ssc.ServerName = "Test Server " + i;
			ServerList.Children.Add(ssc);
			++i;
		}
	}
}
