﻿using Haron.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Haron.Presentation {
	public class PresentationManager {
		private static PresentationManager Instance = null;

		public static PresentationManager GetInstance() {
			if (Instance == null) Instance = new PresentationManager();
			return Instance;
		}

		private List<Type> DashboardItem = new List<Type>();
		private List<Type> MenuItem = new List<Type>();
		private List<Type> ServerItem = new List<Type>();

		private PresentationManager() {

		}

		public static void RegisterDashboardItem(Type item) {

		}

		public static void RegisterMenuItem(Type item) {

		}

		public static void RegisterServerItem(Type item) {
			if (GetInstance().ServerItem.IndexOf(item) == -1 && item.BaseType == typeof(IServer)) 
					GetInstance().ServerItem.Add(item);
		}
	}
}
