﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Haron.Server;
using System.IO;
using System.Windows;
using System.Runtime.InteropServices;
using Haron.Presentation.TeamProject;
using Haron.Utils;
using Haron.Log;
using Haron.Server.Http;
using Haron.Server.Http.MiddleWare;
using Haron.Server.Http.Packet;
using Haron.Settings;
using System.Security.Cryptography.X509Certificates;

namespace Haron {
	public static class Initializer {

		[STAThread]
		static void Main(string[] args) {
			Values.ConsoleInterfaceEnabled = false;
			Values.LoadThirdPartyPlugins = true;
			Values.TestRun = false;
			//당분간 윈도우 폼으로 작업
			Values.UseWindowsForm = true;

			for (int i = 0; i < args.Length; ++i) {
				string arg = args[i];
				if (arg == "-console") {
					Values.ConsoleInterfaceEnabled = true;
				}
				else if(arg == "-test") {
					Values.ConsoleInterfaceEnabled = true;
					Values.TestRun = true;
				}
				else if(arg == "-noplugin") {
					Values.LoadThirdPartyPlugins = false;
				}
				else if(arg == "-nowpf") {
					Values.UseWindowsForm = true;
				}
			}

			string dlldir = AppSettings.Current.PluginPath;
			if (!Directory.Exists(dlldir)) Directory.CreateDirectory(dlldir);
			string[] files = Directory.GetFiles(dlldir);
			//Plugin.PluginManager.LoadInternalPlugins(Environment.UserInteractive, args);
			if (Values.LoadThirdPartyPlugins) Plugin.PluginManager.LoadPlugins(files, args, Environment.UserInteractive);

			if (Values.ConsoleInterfaceEnabled) {
				ConsoleInit();
			}
			else if(Environment.UserInteractive) {
				ApplicationInit();
			} else {
				FileStream stdoutfs = new FileStream("./haron.stdout.log", FileMode.Append, FileAccess.Write);
				FileStream stderrfs = new FileStream("./haron.stderr.log", FileMode.Append, FileAccess.Write);
				ServiceInit(stdoutfs, stderrfs);
			}
		}

		public static void ApplicationInit() {
			if(Values.UseWindowsForm) {
				System.Windows.Forms.Application.EnableVisualStyles();
				System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
				System.Windows.Forms.Application.Run(new MainForm());
			}
			else {
				App.Main();
			}
		}

		public static void ConsoleInit() {
			Kernel32.AllocConsole();

			ServiceInit(Console.OpenStandardOutput(), Console.OpenStandardError());
			while (Console.Read() != 27) { }
		}
		
		public static void ServiceInit(Stream stdout, Stream stderr) {
			LogStream.AddStream(stdout);
			LogStream.Error.AddOutputStream(stderr);
			if (Values.TestRun) {
				HttpServer server = new HttpServer();
				server.OnException += (Exception e) => {
					LogStream.Error.LogException(e, true, true);
				};
				server.Router.RegisterMiddleWare("/", new HelloPage());
				server.OnResponse += (HttpRequest req, HttpResponse res) => {
					LogStream.Log(req.RemoteAddress + " - " + req.URI, true);
;				};
				server.Start();
			}
		}
	}
}
